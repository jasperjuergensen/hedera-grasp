from setuptools import setup
from Cython.Build import cythonize

setup(ext_modules=cythonize("utils/energy_function.pyx"))

import sys
import pandas as pd
import os

from statistics import mean 
from matplotlib import pyplot as plt

from utils.statistics.confidence_interval import number_of_measurements, confidence_interval
from utils.statistics.difference_of_means import difference_of_means
from utils.statistics.means_of_difference import means_of_difference


def get_value_lists(hederaAlgorithm, flowAlgorithm):
    list = []
    dirList = ["./Results/fat-tree-8"]
    for dir in dirList:
        filename = os.path.join(dir, "{}_{}.csv".format(hederaAlgorithm, flowAlgorithm))
        if os.path.isfile(filename):
            list.extend(
                pd.read_csv(filename,header=None,names=["mean","median","bisection"])['bisection'].tolist())
    return list

# Choose
#flowAlg = ("stride 1", "stride_i:1")
#flowAlg = ("stride 2", "stride_i:2")
#flowAlg = ("stride 4", "stride_i:4")
#flowAlg = ("stride 8", "stride_i:8")
#flowAlg = ("staggered 0.2 0.3", "staggered_edgeP:0.2,podP:0.3")
#flowAlg = ("staggered 0.5 0.3", "staggered_edgeP:0.5,podP:0.3")
#flowAlg = ("staggered 1.0 0.0", "staggered_edgeP:1.0,podP:0.0")
#flowAlg = ("random", "random")
flowAlg = ("random bijektive", "random_bijektive")

title = "{} bisection".format(flowAlg[0])
annealing = get_value_lists('annealing',flowAlg[1])
grasp = get_value_lists('grasp',flowAlg[1])
ecmp = get_value_lists('none',flowAlg[1])

print (title)

ALPHA = 0.1
E = 0.1

if len(sys.argv) > 1:
    ALPHA = float(sys.argv[1])

print("confidence level: {}%".format((1 - ALPHA) * 100))
print("error percentage: {}".format(E))

print("number of measurements annealing", number_of_measurements(annealing, ALPHA, E)) 
print("number of measurements grasp", number_of_measurements(grasp, ALPHA, E)) 
print("number of measurements ecmp", number_of_measurements(ecmp, ALPHA, E)) 

confidence_intervals = [
     list(confidence_interval(annealing, ALPHA)),
     list(confidence_interval(grasp, ALPHA)),
     list(confidence_interval(ecmp, ALPHA)) 
] 
print("confidence interval annealing", confidence_intervals[0]) 
print("confidence interval grasp", confidence_intervals[1]) 
print("confidence interval ecmp", confidence_intervals[2]) 

means = [mean(annealing), mean(grasp), mean(ecmp)] 
print("mean annealing", means[0]) 
print("mean grasp", means[1])
print("mean ecmp", means[2])

print("number of measurements annealing", number_of_measurements(annealing, ALPHA, E))
print("number of measurements grasp", number_of_measurements(grasp, ALPHA, E))
print("number of measurements ecmp", number_of_measurements(ecmp, ALPHA, E))

print("confidence interval annealing", confidence_interval(annealing, ALPHA))
print("confidence interval grasp", confidence_interval(grasp, ALPHA))
print("confidence interval ecmp", confidence_interval(ecmp, ALPHA))

print("annealing vs. ecmp")
print("means of difference", means_of_difference(annealing, ecmp, ALPHA))
print("difference of means", difference_of_means(annealing, ecmp, ALPHA))

print("grasp vs. ecmp")
print("means of difference", means_of_difference(grasp, ecmp, ALPHA))
print("difference of means", difference_of_means(grasp, ecmp, ALPHA))

print("annealing vs. grasp")
print("means of difference", means_of_difference(annealing, grasp, ALPHA))
print("difference of means", difference_of_means(annealing, grasp, ALPHA))

plt.bar([0, 1, 2], means, yerr=[
    [means[0] - confidence_intervals[0][0], means[1] - confidence_intervals[1][0], means[2] - confidence_intervals[2][0]],
    [confidence_intervals[0][1] - means[0], confidence_intervals[1][1] - means[1], confidence_intervals[2][1] - means[2]]
])
plt.title(title)
plt.ylabel("Average Bisection Bandwidth")
plt.xticks([0, 1, 2], ("annealing", "grasp", "ecmp"))
plt.show()



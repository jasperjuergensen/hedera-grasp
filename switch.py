from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import MAIN_DISPATCHER, DEAD_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.lib import hub
from ryu.lib.packet import in_proto
from ryu.ofproto import ofproto_v1_3

from config import CONF
from logger import logger
from utils.stats_handling import handle_stats

# Import and define all algorithms
from algorithms import GRASP, SimulatedAnnealing, Base

ALGORITHMS = {"grasp": GRASP, "annealing": SimulatedAnnealing, "none": Base}


class Switch(app_manager.RyuApp):
    """
    Ryu App managing all the different part of hedera
    """

    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    def __init__(self, *args, **kwargs):

        # dict to keep track of all active datapaths
        self.datapaths = {}

        # initialize the choosen hedera algorithm
        if CONF.hedera_algorithm not in ALGORITHMS:
            raise Exception("Unsupported Algorithm in Config")
        self.algorithm = ALGORITHMS.get(CONF.hedera_algorithm)(CONF.iterations)

        # start statistics request thread
        if CONF.hedera_algorithm != "none":
            self.stats_request_thread = hub.spawn(self._stats_request_loop)

        # set with open statistics request for aggregation
        self.open_stats_request = set()

        # dict to aggregate statistics
        self.stats = {}

        # dict to remember all active hedera rules
        self.existing_flow_rules = {}

        super(Switch, self).__init__(*args, **kwargs)

    @set_ev_cls(ofp_event.EventOFPStateChange, [MAIN_DISPATCHER, DEAD_DISPATCHER])
    def _state_change_handler(self, ev):
        """
        Keeps the datapath dictionary up to date
        """
        # Add new datapath if not already in list
        if ev.state == MAIN_DISPATCHER and ev.datapath.id not in self.datapaths:
            self.datapaths[ev.datapath.id] = ev.datapath
        # delete old datapath if in list
        elif ev.state == DEAD_DISPATCHER and ev.datapath.id in self.datapaths:
            del self.datapaths[ev.datapath.id]

    def _stats_request_loop(self):
        """
        To be spawned as thread, periodically sends statistics request to all agg switch
        """

        while True:
            for dp in self.datapaths.values():
                if dp.id >> 56 == 0x02:
                    dp.send_msg(dp.ofproto_parser.OFPFlowStatsRequest(dp))
                    self.open_stats_request.add(dp.id)
            hub.sleep(CONF.stats_frequency)

    @set_ev_cls(ofp_event.EventOFPFlowStatsReply, MAIN_DISPATCHER)
    def _flow_stats_reply_handler(self, ev):
        """
        Handels the incoming statistics and aggregates them
        """

        # get event message body
        body = ev.msg.body

        # get only only for flows from ryu (valid priority) and with src and dst set
        for stat in sorted(
            [
                flow
                for flow in body
                if (
                    (flow.priority not in [0, 65535])
                    and (flow.match.get("ipv4_src"))
                    and (flow.match.get("ipv4_dst"))
                )
            ],
            key=lambda flow: (
                flow.priority,
                flow.match.get("ipv4_src"),
                flow.match.get("ipv4_dst"),
            ),
        ):

            src_ip, dst_ip, src_port, dst_port, l4_proto, ignore = self.flow_rule_key_builder(
                "", stat.match
            )

            # add to aggregation
            self.stats[(src_ip, dst_ip, src_port, dst_port, l4_proto)] = stat

        # remove switch from open requests set
        self.open_stats_request.remove(ev.msg.datapath.id)

        # if all switches reported stats, update paths
        if len(self.open_stats_request) == 0:
            self.aggregated_stats_handler()

    def aggregated_stats_handler(self):
        """
        Applies the algorithm on the aggregated statistics and sets the rules
        """

        # get demand matrix {np.ndarray} and influenced flows from stats handler
        M, influenced_flows = handle_stats(self.stats.values())

        if len(influenced_flows) == 0:
            logger.info("No elephant flows of %d flows", len(self.stats))
            return
        else:
            logger.info("There are %d elephant flows", len(influenced_flows))

        # reset stats aggrgation
        self.stats = {}

        # Apply choosen algorithm on demand matrix
        self.algorithm.update_demand_matrix(M)

        # iterate over all influenced flow and update rules
        for flow in influenced_flows:

            # get src ip of flow
            src_ip = flow.match.get("ipv4_src")

            # get core ip which should be used for that traffic
            core_ip = CONF.host_ip_dict[
                self.algorithm.state[CONF.host_ip_dict[flow.match.get("ipv4_dst")]]
            ]

            logger.info(
                "elephant flow from %s to %s should be over %s",
                flow.match.get("ipv4_src"),
                flow.match.get("ipv4_dst"),
                core_ip,
            )

            # get dp id of influenced aggregation and edge switches
            pod_id = int(src_ip.split(".")[1])
            agg_switch_id = int(core_ip.split(".")[2]) + int(CONF.pod_count / 2) - 1
            edge_switch_id = int(src_ip.split(".")[2])
            agg_dp_id = int(
                "0200000000{:02x}{:02x}{:02x}".format(pod_id, agg_switch_id, 1), 16
            )
            edge_dp_id = int(
                "0300000000{:02x}{:02x}{:02x}".format(pod_id, edge_switch_id, 1), 16
            )

            # update flow table entries for influenced switches
            dp = self.datapaths.get(agg_dp_id)
            if dp is None:
                raise Exception("Switch with datapath id {} not found".format(dp))
            self.set_flow_rule(dp, flow.match, int(core_ip.split(".")[3]))
            dp = self.datapaths.get(edge_dp_id)
            if dp is None:
                raise Exception("Switch with datapath id {} not found".format(dp))
            self.set_flow_rule(dp, flow.match, int(core_ip.split(".")[3]))

    def set_flow_rule(self, dp, match, out_port):
        """
        Adds or updates flow rule

        Arguments:
            dp {int} -- Datapath id of the switch where the rule should be applied
            match {ryu.ofproto.ofproto_v1_3_parser.OFPMatch} -- OpenFlow match object with the matching criteria for the rule
            out_port {int} -- Port where packets matching the rule should be forwarded
        """

        ofp = dp.ofproto
        ofpp = dp.ofproto_parser
        actions = [ofpp.OFPActionOutput(out_port)]
        flow_rule_key = self.flow_rule_key_builder(dp.id, match)
        command = (
            ofp.OFPFC_MODIFY
            if flow_rule_key in self.existing_flow_rules
            else ofp.OFPFC_ADD
        )
        rule = ofpp.OFPFlowMod(
            datapath=dp,
            match=match,
            instructions=[ofpp.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS, actions)],
            idle_timeout=120,
            priority=30000,
            command=command,
        )

        # send rule
        dp.send_msg(rule)
        # update local list
        self.existing_flow_rules[flow_rule_key] = rule

    def flow_rule_key_builder(self, dp_id, match):
        """
        Creates key to save flow rules

        Arguments:
            dp_id {int} -- Datapath id of the switch where the rule is applied on
            match {ryu.ofproto.ofproto_v1_3_parser.OFPMatch} -- OpenFlow match object with the matching criteria for the rule

        Returns:
            Tupel[srt, str, int, int, int, int] -- A key for a rule consisting of
                src_ip, dst_ip, src_port, dst_port, l4_proto and dp_id
        """

        if match.get("ip_proto") == in_proto.IPPROTO_TCP:
            return (
                match.get("ipv4_src"),
                match.get("ipv4_dst"),
                match.get("tcp_src"),
                match.get("tcp_dst"),
                match.get("ip_proto"),
                dp_id,
            )
        elif match.get("ip_proto") == in_proto.IPPROTO_UDP:
            return (
                match.get("ipv4_src"),
                match.get("ipv4_dst"),
                match.get("udp_src"),
                match.get("udp_dst"),
                match.get("ip_proto"),
                dp_id,
            )
        elif match.get("ip_proto") == in_proto.IPPROTO_ICMP:
            return (
                match.get("ipv4_src"),
                match.get("ipv4_dst"),
                match.get("icmpv4_type"),
                match.get("icmpv4_code"),
                match.get("ip_proto"),
                dp_id,
            )
        else:
            raise Exception("Unsupported L4 Protocol")

    @set_ev_cls(ofp_event.EventOFPFlowRemoved, MAIN_DISPATCHER)
    def _flow_removed_handler(self, ev):
        """
        Removes removed flow rules from own list
        """

        # if its a hedera rule
        if ev.msg.priority == 30000:

            # TODO check reason is IDLE_TIMEOUT

            # delete from list
            del self.existing_flow_rules[
                self.flow_rule_key_builder(ev.datapath.id, ev.msg.match)
            ]


# use ecmp as default algorithm
app_manager.require_app("ecmp_application")

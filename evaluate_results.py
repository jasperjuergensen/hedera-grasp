import itertools
import sys

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

from utils.statistics.confidence_interval import confidence_interval
from utils.statistics.means_of_difference import means_of_difference

ALPHA = 0.05
E = 0.1

if len(sys.argv) > 1:
    ALPHA = float(sys.argv[1])

print("confidence level: {}%".format((1 - ALPHA) * 100))

df = pd.read_csv("Results/fat-tree-4/results_new.csv")

DATA_FIELD = "mean"  # one of mean, median, bisection

ALGO_LABEL = {"annealing": "Simulated Annealing", "grasp": "GRASP", "ecmp": "ECMP"}
TEST_LABEL = {"random": "rand", "random_bijektive": "randbij", "staggered0203": "stag(0.2,0.3)", "staggered0503": "stag(0.5,0.3)", "staggered1000": "stag(1.0,0.0)"}

for test, test_data in df.groupby("test"):
    print(test)
    test_group_data = list()
    test_group_text = list()
    for algo, test_algo_data in test_data.groupby("algo"):
        test_group_data.append(test_algo_data[DATA_FIELD])
        test_group_text.append(algo)
    for a, b in itertools.combinations(list(range(len(test_group_text))), 2):
        print("{} vs. {} means of difference: {}".format(
            test_group_text[a], test_group_text[b], means_of_difference(test_group_data[a], test_group_data[b], ALPHA))
        )

plt.figure(figsize=(10, 5))
ax = plt.subplot()
algo_id = 0
metrics = list()
for algo, algo_data in df.groupby("algo"):
    means = list()
    lower = list()
    upper = list()
    metrics = list()
    for test, test_algo_data in algo_data.groupby("test"):
        metrics.append(TEST_LABEL.get(test, test))
        means.append(test_algo_data[DATA_FIELD].mean() / 1000000)
        lower.append((test_algo_data[DATA_FIELD].mean() - confidence_interval(test_algo_data[DATA_FIELD], ALPHA)[0]) / 1000000)
        upper.append((confidence_interval(test_algo_data[DATA_FIELD], ALPHA)[1] - test_algo_data[DATA_FIELD].mean()) / 1000000)
    ax.bar(np.arange(algo_id, 8 * 5 + algo_id, 5), means, yerr=[lower, upper], width=1, label=ALGO_LABEL[algo])
    algo_id += 1
plt.xticks(np.arange(1, 8 * 5 + 1, 5), tuple(metrics))
plt.title("Simulated Annealing vs. GRASP vs. ECMP, k=4")
plt.ylabel("Average {} bandwidth (Mbps)".format(DATA_FIELD))
plt.legend(loc=2)
plt.savefig("Results/fat-tree-4/graphs/annealing_vs_grasp_vs_ecmp_mean.png", transparent=True)
#plt.show()


# print("annealing 100 vs. 1000")
# from statistics import mean
# means_100 = list()
# err_100_0 = list()
# err_100_1 = list()
# means_1000 = list()
# err_1000_0 = list()
# err_1000_1 = list()
# means_200 = list()
# err_200_0 = list()
# err_200_1 = list()
# print("stride 1")
# annealing_100 = [9364607.75,9363654.5,9368469.10938,9364161.40625,9362858.09375,9363726.92188,9360631.73438,9360849.09375,9359853.375,9361784.24479]
# #annealing_1000 = [3332502.41391,3332511.58643,3332522.7593,3332498.82094,3332499.22073,3332491.24725,3332493.63774,3332489.64704,3332516.77617,3332503.61054]
# annealing_1000 = [9363630.39583,9363473.48438,9365935.04688,9364094.92708,9363328.71875,9363111.45312,9361808.375,9363039.07812,9363002.85938,9362043.57812]
# #annealing_200 = [3157015.18974,3194152.60572,3109559.14153,3131289.4542,3135988.47486,3122788.14291,3158980.95558,3221631.88809,3218249.23382,3205001.03271]
# annealing_200 = [9362164.26562,9362122.00521,9363708.84375,9363159.69271,9373102.6875,9362025.53125,9362471.94792,9362200.46875,9362622.79688,9349808.01562]
# print("confidence interval 100", confidence_interval(annealing_100, ALPHA))
# print("confidence interval 200", confidence_interval(annealing_200, ALPHA))
# print("confidence interval 1000", confidence_interval(annealing_1000, ALPHA))
# print("mean 100", mean(annealing_100))
# print("mean 200", mean(annealing_200))
# print("mean 1000", mean(annealing_1000))
# print("100 vs. 200 means of difference", means_of_difference(annealing_100, annealing_200, ALPHA))
# print("100 vs. 1000 means of difference", means_of_difference(annealing_100, annealing_1000, ALPHA))
# print("200 vs. 1000 means of difference", means_of_difference(annealing_200, annealing_1000, ALPHA))
# means_100.append(mean(annealing_100) / 1000000)
# means_1000.append(mean(annealing_1000) / 1000000)
# means_200.append(mean(annealing_200) / 1000000)
# err_100_0.append((mean(annealing_100) - confidence_interval(annealing_100, ALPHA)[0]) / 1000000)
# err_100_1.append((confidence_interval(annealing_100, ALPHA)[1] - mean(annealing_100)) / 1000000)
# err_1000_0.append((mean(annealing_1000) - confidence_interval(annealing_1000, ALPHA)[0]) / 1000000)
# err_1000_1.append((confidence_interval(annealing_1000, ALPHA)[1] - mean(annealing_1000)) / 1000000)
# err_200_0.append((mean(annealing_200) - confidence_interval(annealing_200, ALPHA)[0]) / 1000000)
# err_200_1.append((confidence_interval(annealing_200, ALPHA)[1] - mean(annealing_200)) / 1000000)
#
# print("stride 2")
# annealing_100 = [9197478.42188,7811356.28646,6026202.79688,5767887.85417,6369220.65104,8652324.42188,5186349.52604,5256023.63542,5846293.57292,6330665.86458]
# #annealing_1000 = [3250202.5699,3137218.67872,3289487.64428,3138901.26136,3077640.08919,3136653.83574,3052795.8936,3090949.69525,3154945.70213,3102857.5947]
# annealing_1000 = [6449626.91146,7705187.71875,6701982.69792,5124971.44792,5733811.4375,9278765.51562,5680020.90104,6340495.94271,6949317.52083,5727844.73438]
# #annealing_200 = [3199638.2407,3209704.83712,3156603.52307,3153801.66219,3204426.62087,3099518.01894,3121431.88395,3167428.02583,3173686.76687,3192628.80682]
# annealing_200 = [7387291.38021,4714930.21354,5325756.375,6756510.06771,5970613.375,6062914.75,8561039.45312,5332803.58333,8097957.71875,5370919.73438]
# print("confidence interval 100", confidence_interval(annealing_100, ALPHA))
# print("confidence interval 200", confidence_interval(annealing_200, ALPHA))
# print("confidence interval 1000", confidence_interval(annealing_1000, ALPHA))
# print("mean 100", mean(annealing_100))
# print("mean 200", mean(annealing_200))
# print("mean 1000", mean(annealing_1000))
# print("100 vs. 200 means of difference", means_of_difference(annealing_100, annealing_200, ALPHA))
# print("100 vs. 1000 means of difference", means_of_difference(annealing_100, annealing_1000, ALPHA))
# print("200 vs. 1000 means of difference", means_of_difference(annealing_200, annealing_1000, ALPHA))
# means_100.append(mean(annealing_100) / 1000000)
# means_1000.append(mean(annealing_1000) / 1000000)
# means_200.append(mean(annealing_200) / 1000000)
# err_100_0.append((mean(annealing_100) - confidence_interval(annealing_100, ALPHA)[0]) / 1000000)
# err_100_1.append((confidence_interval(annealing_100, ALPHA)[1] - mean(annealing_100)) / 1000000)
# err_1000_0.append((mean(annealing_1000) - confidence_interval(annealing_1000, ALPHA)[0]) / 1000000)
# err_1000_1.append((confidence_interval(annealing_1000, ALPHA)[1] - mean(annealing_1000)) / 1000000)
# err_200_0.append((mean(annealing_200) - confidence_interval(annealing_200, ALPHA)[0]) / 1000000)
# err_200_1.append((confidence_interval(annealing_200, ALPHA)[1] - mean(annealing_200)) / 1000000)
#
# print("stride 4")
# annealing_100 = [6252064.22396,5437240.01042,6893782.89062,6741067.85938,5749933.14583,6723834.72917,6551445.40104,6816448.23958,6821683.47917,7524336.05208]
# #annealing_1000 = [3150653.56853,3184245.63051,3161748.54924,3152032.15393,3227736.62362,3156204.625,3179541.76274,3147559.70902,3115730.83712,3119779.052]
# annealing_1000 = [6714852.27083,7855912.39583,7008561.40104,7474105.02083,6657703.06771,6301948.63542,6112091.33333,5991514.15104,6496186.16667,6396311.71875]
# #annealing_200 = [3174281.09056,3143143.10847,3114044.29167,3206154.7104,3190864.08781,3209254.1126,3161618.47486,3169923.59194,3186340.58506,3131669.2104]
# annealing_200 = [7701169.10938,6131764.44792,6437674.76042,6201339.98958,6716452.16667,6860307.63021,6588566.76042,6471950.99479,8167009.20312,7138031.79167]
# print("confidence interval 100", confidence_interval(annealing_100, ALPHA))
# print("confidence interval 200", confidence_interval(annealing_200, ALPHA))
# print("confidence interval 1000", confidence_interval(annealing_1000, ALPHA))
# print("mean 100", mean(annealing_100))
# print("mean 200", mean(annealing_200))
# print("mean 1000", mean(annealing_1000))
# print("100 vs. 200 means of difference", means_of_difference(annealing_100, annealing_200, ALPHA))
# print("100 vs. 1000 means of difference", means_of_difference(annealing_100, annealing_1000, ALPHA))
# print("200 vs. 1000 means of difference", means_of_difference(annealing_200, annealing_1000, ALPHA))
# means_100.append(mean(annealing_100) / 1000000)
# means_1000.append(mean(annealing_1000) / 1000000)
# means_200.append(mean(annealing_200) / 1000000)
# err_100_0.append((mean(annealing_100) - confidence_interval(annealing_100, ALPHA)[0]) / 1000000)
# err_100_1.append((confidence_interval(annealing_100, ALPHA)[1] - mean(annealing_100)) / 1000000)
# err_1000_0.append((mean(annealing_1000) - confidence_interval(annealing_1000, ALPHA)[0]) / 1000000)
# err_1000_1.append((confidence_interval(annealing_1000, ALPHA)[1] - mean(annealing_1000)) / 1000000)
# err_200_0.append((mean(annealing_200) - confidence_interval(annealing_200, ALPHA)[0]) / 1000000)
# err_200_1.append((confidence_interval(annealing_200, ALPHA)[1] - mean(annealing_200)) / 1000000)
#
# print("stride 8")
# annealing_100 = [6168653.89062,8334289.17188,6892487.6875,7521385.41146,6217233.53125,6722742.86979,6846409.01042,7542300.29688,6680376.625,7792290.94792]
# #annealing_1000 = [3198459.10158,3216824.37018,3192226.72142,3257459.32472,3109656.41701,3171870.15255,3155722.82472,3152435.07679,3207978.47073,3127839.78478]
# annealing_1000 = [7343247.04688,6946574.53646,7629611.82812,7833742.66146,6415424.79167,6872338.4375,6064031.30729,7747795.09375,6933512.23438,7486564.20312]
# #annealing_200 = [3208956.50379,3161195.64015,3184095.57679,3144929.40324,3183388.73519,3130413.47624,3126973.3812,3184610.95971,3199928.64842,3131823.97624]
# annealing_200 = [8303240.48958,6625965.625,6322844.98958,8238666.42188,7108424.65104,7102633.71875,5569881.23438,6419722.05729,7200623.11458,6097961.33854]
# print("confidence interval 100", confidence_interval(annealing_100, ALPHA))
# print("confidence interval 200", confidence_interval(annealing_200, ALPHA))
# print("confidence interval 1000", confidence_interval(annealing_1000, ALPHA))
# print("mean 100", mean(annealing_100))
# print("mean 200", mean(annealing_200))
# print("mean 1000", mean(annealing_1000))
# print("100 vs. 200 means of difference", means_of_difference(annealing_100, annealing_200, ALPHA))
# print("100 vs. 1000 means of difference", means_of_difference(annealing_100, annealing_1000, ALPHA))
# print("200 vs. 1000 means of difference", means_of_difference(annealing_200, annealing_1000, ALPHA))
# means_100.append(mean(annealing_100) / 1000000)
# means_1000.append(mean(annealing_1000) / 1000000)
# means_200.append(mean(annealing_200) / 1000000)
# err_100_0.append((mean(annealing_100) - confidence_interval(annealing_100, ALPHA)[0]) / 1000000)
# err_100_1.append((confidence_interval(annealing_100, ALPHA)[1] - mean(annealing_100)) / 1000000)
# err_1000_0.append((mean(annealing_1000) - confidence_interval(annealing_1000, ALPHA)[0]) / 1000000)
# err_1000_1.append((confidence_interval(annealing_1000, ALPHA)[1] - mean(annealing_1000)) / 1000000)
# err_200_0.append((mean(annealing_200) - confidence_interval(annealing_200, ALPHA)[0]) / 1000000)
# err_200_1.append((confidence_interval(annealing_200, ALPHA)[1] - mean(annealing_200)) / 1000000)
#
# ax = plt.subplot()
# ax.bar([0, 2, 4, 6], means_100, yerr=[err_100_0, err_100_1], width=0.5, label="100 iterations")
# ax.bar([0.5, 2.5, 4.5, 6.5], means_200, yerr=[err_200_0, err_200_1], width=0.5, label="200 iterations")
# ax.bar([1, 3, 5, 7], means_1000, yerr=[err_1000_0, err_1000_1], width=0.5, label="1000 iterations")
# plt.xticks([0.25, 2.25, 4.25, 6.25], ("stride 1", "stride 2", "stride 4", "stride 8"))
# plt.title("Simulated Annealing 100 vs. 200 vs. 1000 iterations")
# plt.ylabel("Average mean bandwidth (Mbps)")
# plt.legend(loc=1)
# plt.savefig("Results/fat-tree-4/graphs/annealing_100_vs_1000_vs_200_mean.png", transparent=True)
# #plt.show()
#
#
#
# print("grasp 100 vs. 200 vs. 30")
# from statistics import mean
# means_100 = list()
# err_100_0 = list()
# err_100_1 = list()
# means_200 = list()
# err_200_0 = list()
# err_200_1 = list()
# means_200_30 = list()
# err_200_30_0 = list()
# err_200_30_1 = list()
# print("stride 1")
# grasp_100 = [9364523.23438,9364360.39062,9360631.76562,9365591.21875,9360867.125,9363998.38542,9362785.73438,9363057.19792,9362586.54688,9363093.39062]
# grasp_200 = [9362833.90625,9363570,9362459.98438,9362930.53125,9365030.10938,9362097.82812,9361029.98438,9363775.20312,9355002.71875,9330881.43229]
# grasp_200_30 = [9364233.73438,9362622.8125,9362043.57812,9366206.64062,9369446.5625,9358767.54688,9362785.67188,9350405.29688,9330169.5625,9363805.34896]
# print("confidence interval 100", confidence_interval(grasp_100, ALPHA))
# print("confidence interval 5000", confidence_interval(grasp_200, ALPHA))
# print("mean 100", mean(grasp_100))
# print("mean 200", mean(grasp_200))
# print("100 vs. 200 means of difference", means_of_difference(grasp_100, grasp_200, ALPHA))
# print("200 vs. 200, 30 means of difference", means_of_difference(grasp_200, grasp_200_30, ALPHA))
# means_100.append(mean(grasp_100) / 1000000)
# means_200.append(mean(grasp_200) / 1000000)
# means_200_30.append(mean(grasp_200_30) / 1000000)
# err_100_0.append((mean(grasp_100) - confidence_interval(grasp_100, ALPHA)[0]) / 1000000)
# err_100_1.append((confidence_interval(grasp_100, ALPHA)[1] - mean(grasp_100)) / 1000000)
# err_200_0.append((mean(grasp_200) - confidence_interval(grasp_200, ALPHA)[0]) / 1000000)
# err_200_1.append((confidence_interval(grasp_200, ALPHA)[1] - mean(grasp_200)) / 1000000)
# err_200_30_0.append((mean(grasp_200_30) - confidence_interval(grasp_200_30, ALPHA)[0]) / 1000000)
# err_200_30_1.append((confidence_interval(grasp_200_30, ALPHA)[1] - mean(grasp_200_30)) / 1000000)
#
# print("stride 2")
# grasp_100 = [7542981.48438,7883305.85938,6590557.20833,8940639.4375,7909691.01042,7484599.65625,5241205.54167,4910409.41146,6859746.29688,5978423.82812]
# grasp_200 = [7410664.52604,6721167.1875,6198628.30208,7702995.22396,7681135.42188,6590125.15625,4850025.95833,5749844.72917,6970621.15625,5949130.66146]
# grasp_200_30 = [7019656.20312,5829780.32292,8531367.42188,7674279.40104,7999566.0625,7430705.21354,5873986.09375,5605112.65625,8698606.16667,5673846.34375]
# print("confidence interval 100", confidence_interval(grasp_100, ALPHA))
# print("confidence interval 5000", confidence_interval(grasp_200, ALPHA))
# print("mean 100", mean(grasp_100))
# print("mean 200", mean(grasp_200))
# print("100 vs. 200 means of difference", means_of_difference(grasp_100, grasp_200, ALPHA))
# print("200 vs. 200, 30 means of difference", means_of_difference(grasp_200, grasp_200_30, ALPHA))
# means_100.append(mean(grasp_100) / 1000000)
# means_200.append(mean(grasp_200) / 1000000)
# means_200_30.append(mean(grasp_200_30) / 1000000)
# err_100_0.append((mean(grasp_100) - confidence_interval(grasp_100, ALPHA)[0]) / 1000000)
# err_100_1.append((confidence_interval(grasp_100, ALPHA)[1] - mean(grasp_100)) / 1000000)
# err_200_0.append((mean(grasp_200) - confidence_interval(grasp_200, ALPHA)[0]) / 1000000)
# err_200_1.append((confidence_interval(grasp_200, ALPHA)[1] - mean(grasp_200)) / 1000000)
# err_200_30_0.append((mean(grasp_200_30) - confidence_interval(grasp_200_30, ALPHA)[0]) / 1000000)
# err_200_30_1.append((confidence_interval(grasp_200_30, ALPHA)[1] - mean(grasp_200_30)) / 1000000)
#
# print("stride 4")
# grasp_100 = [6404701.46354,6829527.71875,7120208.14062,6751133.88542,7185576.83333,7189171.65625,7401736.40625,7286400.66146,7182216.94792,6973618.24479]
# grasp_200 = [7263980.4375,6401254.63021,6824321.33854,7143350.59375,7744603.51562,5960700.04688,5502670.89062,6663792.55729,7386743.51042,6671844.71875]
# grasp_200_30 = [6622938.02604,7237994.07292,6073813.05729,8111795.83333,7617963.09896,7271654.65625,6829516.55208,5974403.20312,6756622.94271,6316259.25521]
# print("confidence interval 100", confidence_interval(grasp_100, ALPHA))
# print("confidence interval 5000", confidence_interval(grasp_200, ALPHA))
# print("mean 100", mean(grasp_100))
# print("mean 200", mean(grasp_200))
# print("100 vs. 200 means of difference", means_of_difference(grasp_100, grasp_200, ALPHA))
# print("200 vs. 200, 30 means of difference", means_of_difference(grasp_200, grasp_200_30, ALPHA))
# means_100.append(mean(grasp_100) / 1000000)
# means_200.append(mean(grasp_200) / 1000000)
# means_200_30.append(mean(grasp_200_30) / 1000000)
# err_100_0.append((mean(grasp_100) - confidence_interval(grasp_100, ALPHA)[0]) / 1000000)
# err_100_1.append((confidence_interval(grasp_100, ALPHA)[1] - mean(grasp_100)) / 1000000)
# err_200_0.append((mean(grasp_200) - confidence_interval(grasp_200, ALPHA)[0]) / 1000000)
# err_200_1.append((confidence_interval(grasp_200, ALPHA)[1] - mean(grasp_200)) / 1000000)
# err_200_30_0.append((mean(grasp_200_30) - confidence_interval(grasp_200_30, ALPHA)[0]) / 1000000)
# err_200_30_1.append((confidence_interval(grasp_200_30, ALPHA)[1] - mean(grasp_200_30)) / 1000000)
#
# print("stride 8")
# grasp_100 = [6315394.09896,6884823.30729,7101731.32812,7354212.97917,7627044.8125,7261503.05729,6239161.15625,6746015.75521,7569905.54167,7169573.58854]
# grasp_200 = [6606517.71875,6793017.67708,7095023.35938,6854583.79688,7155613.60938,5523792.9375,7067861.125,7329878.71875,6391243.31771,7172100.47917]
# grasp_200_30 = [8053763.56771,6891637.28646,6850448.33333,7155020.82812,7407159.17708,8543573.63542,6796218.64062,7432155.23438,6448577.9375,7243075.08333]
# print("confidence interval 100", confidence_interval(grasp_100, ALPHA))
# print("confidence interval 5000", confidence_interval(grasp_200, ALPHA))
# print("mean 100", mean(grasp_100))
# print("mean 200", mean(grasp_200))
# print("100 vs. 200 means of difference", means_of_difference(grasp_100, grasp_200, ALPHA))
# print("200 vs. 200, 30 means of difference", means_of_difference(grasp_200, grasp_200_30, ALPHA))
# means_100.append(mean(grasp_100) / 1000000)
# means_200.append(mean(grasp_200) / 1000000)
# means_200_30.append(mean(grasp_200_30) / 1000000)
# err_100_0.append((mean(grasp_100) - confidence_interval(grasp_100, ALPHA)[0]) / 1000000)
# err_100_1.append((confidence_interval(grasp_100, ALPHA)[1] - mean(grasp_100)) / 1000000)
# err_200_0.append((mean(grasp_200) - confidence_interval(grasp_200, ALPHA)[0]) / 1000000)
# err_200_1.append((confidence_interval(grasp_200, ALPHA)[1] - mean(grasp_200)) / 1000000)
# err_200_30_0.append((mean(grasp_200_30) - confidence_interval(grasp_200_30, ALPHA)[0]) / 1000000)
# err_200_30_1.append((confidence_interval(grasp_200_30, ALPHA)[1] - mean(grasp_200_30)) / 1000000)
#
# ax = plt.subplot()
# ax.bar([0, 2, 4, 6], means_100, yerr=[err_100_0, err_100_1], width=0.5, label="100 iterations, 10 neighbours")
# ax.bar([0.5, 2.5, 4.5, 6.5], means_200, yerr=[err_200_0, err_200_1], width=0.5, label="200 iterations, 10 neighbours")
# ax.bar([1, 3, 5, 7], means_200_30, yerr=[err_200_30_0, err_200_30_1], width=0.5, label="200 iterations, 30 neighbours")
# plt.xticks([0.25, 2.25, 4.25, 6.25], ("stride 1", "stride 2", "stride 4", "stride 8"))
# plt.title("GRASP 100 vs. 200 iterations, 10 vs. 30 neighbours")
# plt.ylabel("Average mean bandwidth (Mbps)")
# plt.legend()
# plt.savefig("Results/fat-tree-4/graphs/grasp_100_vs_200_vs_30_mean.png", transparent=True)
# #plt.show()

#!/bin/bash

set -e

last_algo="annealing"
last_params="i:1"
for algo in {"annealing","grasp","none"}; do
    sed -i "s/^algorithm = ${last_algo}$/algorithm = ${algo}/" params.conf
    last_algo=$algo

    for params in {"i:1","i:2","i:4","i:8"}; do
        sed -i "s/^test_params = ${last_params}$/test_params = ${params}/" params.conf
        last_params=$params
        for i in {1..10}; do
            sudo python run.py --config-file params.conf
            python iperf_log_parser.py logging/server/ | tail -n 1 >>"results/${algo}_stride_${params}.csv"
        done
    done

    sed -i "s/^test = stride$/test = staggered/" params.conf

    for params in {"edgeP:0.2,podP:0.3","edgeP:0.5,podP:0.3"}; do
        for seed in 5764 1883 4958 6765 527 1546 9787 3015 6806 5345; do
            seed_params="${params},seed:${seed}"
            sed -i "s/^test_params = ${last_params}$/test_params = ${seed_params}/" params.conf
            last_params=seed_params
            sudo python run.py --config-file params.conf
            python iperf_log_parser.py logging/server/ | tail -n 1 >>"results/${algo}_staggered_${params}.csv"
        done
    done

    sed -i "s/^test = staggered$/test = random/" params.conf
    for seed in 5764 1883 4958 6765 527 1546 9787 3015 6806 5345; do
        params="seed:${seed}"
        sed -i "s/^test_params = ${last_params}$/test_params = ${params}/" params.conf
        last_params=$params
        sudo python run.py --config-file params.conf
        python iperf_log_parser.py logging/server/ | tail -n 1 >>"results/${algo}_random.csv"
    done

    sed -i "s/^test = random$/test = random_bijektive/" params.conf
    for seed in 5764 1883 4958 6765 527 1546 9787 3015 6806 5345; do
        params="seed:${seed}"
        sed -i "s/^test_params = ${last_params}$/test_params = ${params}/" params.conf
        last_params=$params
        sudo python run.py --config-file params.conf
        python iperf_log_parser.py logging/server/ | tail -n 1 >>"results/${algo}_random_bijektive.csv"
    done
    sed -i "s/^test = random_bijektive$/test = stride/" params.conf
done

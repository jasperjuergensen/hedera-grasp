#!/bin/bash

set -e

for algo in {"annealing","grasp","none"}; do

    for params in {"i:1","i:2","i:4","i:8"}; do
        for i in {1..10}; do
            sudo python run.py --config-file "params_${algo}_stride_${params}.conf"
            python iperf_log_parser.py logging/server/ | tail -n 1 >>"results/${algo}_stride_${params}.csv"
        done
    done

    for params in {"edgeP:0.2,podP:0.3","edgeP:0.5,podP:0.3"}; do
        for seed in 7835 8501 5567 9414 2470 5440 5181 455 5248 7362; do
            seed_params="${params},seed:${seed}"
            sudo python run.py --config-file "params_${algo}_staggered_${seed_params}.conf"
            python iperf_log_parser.py logging/server/ | tail -n 1 >>"results/${algo}_staggered_${params}.csv"
        done
    done

    for seed in 7835 8501 5567 9414 2470 5440 5181 455 5248 7362; do
        params="seed:${seed}"
        sudo python run.py --config-file "params_${algo}_random_${params}.conf"
        python iperf_log_parser.py logging/server/ | tail -n 1 >>"results/${algo}_random.csv"
    done

    for seed in 7835 8501 5567 9414 2470 5440 5181 455 5248 7362; do
        params="seed:${seed}"
        sudo python run.py --config-file "params_${algo}_random_bijektive_${params}.conf"
        python iperf_log_parser.py logging/server/ | tail -n 1 >>"results/${algo}_random_bijektive.csv"
    done
done

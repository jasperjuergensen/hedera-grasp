import logging
import sys

logger = logging.getLogger("hedera")
logger.setLevel(logging.INFO)
handler = logging.StreamHandler(sys.stdout)
logger.addHandler(handler)

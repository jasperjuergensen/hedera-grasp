import math
import random

from algorithms.base import Base
from config import CONF
from utils.energy_function import energy_function
from utils.utils import switch_cores


class SimulatedAnnealing(Base):
    def __init__(self, max_iterations):
        Base.__init__(self, max_iterations)
        self.c = 0.5 * max_iterations
        self.temperature = max_iterations
        # the initial state distributes the hosts in a pod over the core switches
        # in a fat-tree this should be a 1:1 mapping per pod
        self.state = {
            CONF.flat_host_list[pod_id * CONF.pod_size + host_id]: core
            for pod_id in range(CONF.pod_count)
            for host_id, core in zip(range(CONF.pod_size), CONF.core_list)
        }

    def update_demand_matrix(self, new_demand_matrix):
        # reset temperature to max_iterations
        self.temperature = self.max_iterations
        Base.update_demand_matrix(self, new_demand_matrix)

    def calculate_routes(self):
        state = self.state.copy()

        # get the current energy for the current state
        energy = energy_function(state, self.demand_matrix)

        # at the beginning the current_energy is the best
        best_energy = energy

        # at the beginning the current state is the best
        best_state = state

        # set the start temperature to n and go down to 0
        while self.temperature > 0:
            # get a neighbour state and its energy
            neighbour_state = SimulatedAnnealing.neighbour_generator(state)
            neighbour_energy = energy_function(neighbour_state, self.demand_matrix)

            if neighbour_energy < best_energy:
                # if the current energy is better than the best one
                # set best energy and state to neighbour energy and state
                best_energy = neighbour_energy
                best_state = neighbour_state

            if self.acceptance_probability(energy, neighbour_energy) > random.random():
                state = neighbour_state
                energy = neighbour_energy
            self.temperature -= 1

        return best_state

    @staticmethod
    def neighbour_generator1(state):
        """Neighbour generator

        This neighbour generation function swaps the assigned core
        switches for any two randomly chosen hosts in a randomly chosen pod

        Arguments:
            state {dict} -- The state to calculate the neighbours for

        Returns:
            dict -- The new state
        """

        # choose randomly two hosts from a randomly choosen pod
        pod_id = random.randint(0, CONF.pod_count - 1)
        h1, h2 = random.sample(
            CONF.flat_host_list[pod_id * CONF.pod_size : (pod_id + 1) * CONF.pod_size],
            k=2,
        )

        # change core switches for these two hosts
        state = switch_cores(h1, h2, state.copy())

        return state

    @staticmethod
    def neighbour_generator2(state):
        """Neighbour generator

        This neighbour generation function swaps the assigned core switches for any
        two randomly chosen hosts in a randomly chosen edge switch

        Arguments:
            state {dict} -- The state to calculate the neighbours for

        Returns:
            dict -- The new state
        """

        # choose a random pod id
        pod_id = random.randint(0, CONF.pod_count - 1)

        # choose a random edge switch in this pod
        edge_id = random.randint(0, int(math.sqrt(CONF.pod_size)) - 1)

        # get all hosts for this edge switch
        hosts = [
            host
            for host in CONF.flat_host_list[
                pod_id * CONF.pod_size : (pod_id + 1) * CONF.pod_size
            ]
            if host.startswith("h{:02d}{:02d}".format(pod_id, edge_id))
        ]

        # get two random hosts with the edge switch
        h1, h2 = random.sample(hosts, k=2)

        # change core switches for these two hosts
        state = switch_cores(h1, h2, state.copy())

        return state

    @staticmethod
    def neighbour_generator3(state):
        """Neighbour generator

        This neighbour generation function randomly chooses an
        edge or aggregation switch with equal probability and
        swaps the assigned core switches for a random pair of
        hosts that use the chosen edge or aggregation switch to
        reach their currently assigned core switches

        Arguments:
            state {dict} -- The state to calculate the neighbours for

        Returns:
            dict -- The new state
        """

        # get a single core or aggregation switch
        switch = random.choice(CONF.switch_list)

        if switch.startswith("se"):
            # is edge switch
            # get pod and edge id
            pod_id = int(switch.split("-")[1])
            edge_id = int(switch.split("-")[2])
            # get all hosts for this edge switch (same es ng2)
            hosts = [
                host
                for host in CONF.flat_host_list[
                    pod_id * CONF.pod_size : (pod_id + 1) * CONF.pod_size
                ]
                if host.startswith("h{:02d}{:02d}".format(pod_id, edge_id))
            ]
        else:
            # agg switch
            # get pod and agg id
            pod_id = int(switch.split("-")[1])
            agg_id = int(switch.split("-")[2]) - CONF.pod_edge_count
            # get cores for this agg switch
            core_groups = int(math.sqrt(CONF.core_count))
            core_ids = range(agg_id * core_groups, (agg_id + 1) * core_groups)
            # get hosts that use this core switch and are in the selected pod
            hosts = [
                host
                for host, core in state.items()
                if any(core.endswith(str(core_id)) for core_id in core_ids)
                and host.startswith("h{:02d}".format(pod_id))
            ]

        # get two random hosts with the edge switch
        h1, h2 = random.sample(hosts, k=2)

        # change core switches for these two hosts
        state = switch_cores(h1, h2, state.copy())

        return state

    @staticmethod
    def neighbour_generator(state):
        """Neighbour generator

        Generates a new neighbour state

        Arguments:
            state {dict} -- The state to calculate the neighbours for

        Returns:
            dict -- The neighbour state
        """

        # There are 3 neighbour generation function. This chooses one of them randomly
        # with an equal probability for each one and then uses the choosen function
        neigh_gen_function = random.choice(
            [
                SimulatedAnnealing.neighbour_generator1,
                SimulatedAnnealing.neighbour_generator2,
                SimulatedAnnealing.neighbour_generator3,
            ]
        )
        neighbour_state = neigh_gen_function(state)
        return neighbour_state

    def acceptance_probability(self, current_energy, neighbour_energy):
        """Acceptence probability

        Calculates the acceptence probability for a new state

        Arguments:
            current_energy {float} -- The current energy
            neighbour_energy {float} -- The energy of the new state

        Returns:
            number -- The acceptence probability
        """

        # if the energy of the new state is better than the energy of the current stat
        # then the acceptence probability is 1 (always accept better states)
        if neighbour_energy < current_energy:
            return 1

        # otherwise use this formula to calculate the acceptence probability
        return math.exp(self.c * (current_energy - neighbour_energy) / self.temperature)

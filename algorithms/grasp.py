import random

from algorithms.base import Base
from algorithms.simulated_annealing import SimulatedAnnealing
from config import CONF
from utils.energy_function import energy_function


class GRASP(Base):
    def __init__(self, max_iterations, alpha=0.4):
        Base.__init__(self, max_iterations)
        # check alpha values
        if alpha < 0:
            raise ValueError("alpha cannot be smaller than 0")
        if alpha > 1:
            raise ValueError("alpha cannot be greater than 1")
        self.alpha = alpha

    def calculate_routes(self):
        # Create the first state
        state = self.construction()
        best_state, best_energy = self.local_search(state)

        for i in range(1, self.max_iterations):
            state, energy = self.round()
            if energy < best_energy:
                # If the current solution is the new best state, save it
                best_state = state
                best_energy = energy

        return best_state

    def round(self):
        """
        Runs a single GRASP round (construction + local search)

        Returns:
            Tuple[dict, float] -- The best state and its energy in this round
        """
        state = self.construction()
        state, energy = self.local_search(state)
        return state, energy

    def construction(self):
        """Construction phase

        Greedily create a random feasible solution

        Returns:
            dict -- A random and feasible solution
        """
        # generate random start element
        start_host = random.choice(CONF.flat_host_list)
        state = {start_host: random.choice(CONF.core_list)}

        # for all other elements
        for host in CONF.flat_host_list:
            if host == start_host:
                # skip the start host
                continue
            # create the restricted candidate list
            rcl = self.make_rcl(host, state)
            # select randomly one from the rcl
            choice = random.choice(rcl)
            state[host] = choice

        return state

    def local_search(self, state):
        """Local search phase

        Tries to improve the given solution by running a local search

        Arguments:
            state {dict} -- The state created in the construction phase to perform a local search on

        Returns:
            Tuple[dict, float] -- The local optimum for the initialization state and its energy
        """
        # get the energy of the initialization state
        best_energy = energy_function(state, self.demand_matrix)
        best_state = state

        # flag if a better solution was found
        flag = True

        # while a better solution is found
        while flag:
            flag = False

            # generate some random neighbour states and their energy (TODO maybe generate all neighbour states?)
            neighbour_states = [GRASP.neighbour_generator(state) for _ in range(10)]
            neighbour_energies = [
                energy_function(state, self.demand_matrix) for state in neighbour_states
            ]
            neighbours = zip(neighbour_states, neighbour_energies)

            # get the neighbour with the best energy
            min_neighbour = min(neighbours, key=lambda x: x[1])

            if min_neighbour[1] < best_energy:
                # If the neighbour with the best energy is a better solution, take it
                flag = True
                best_state, best_energy = min_neighbour
                state = best_state
        return best_state, best_energy

    def make_rcl(self, host, orig_state):
        """Make RCL

        Create the restricted candidate list.
        This implementation uses the minmax alpha variant.

        Arguments:
            host {str} -- The host to calculate core candidates for
            orig_state {dict} -- The state in which we need to create candidates

        Returns:
            list -- A list of candidates
        """
        # each core is a possible candidate
        candidates = CONF.core_list
        energies = list()

        # for each candidate
        for candidate in candidates:
            # create a copy of the state to prevent changes to the original
            state = orig_state.copy()

            # set candidate to state
            state[host] = candidate
            energies.append(energy_function(state, self.demand_matrix))

        # calculate min and max energy and the border using alpha
        min_energy = min(energies)
        max_energy = max(energies)
        border = min_energy + (1 - self.alpha) * (max_energy - min_energy)

        # return all candidates with an erergy lower than the border
        rcl = [
            candidate
            for candidate, energy in zip(candidates, energies)
            if energy <= border
        ]
        return rcl

    @staticmethod
    def neighbour_generator(state):
        """Neighbour generator

        Generates a new neighbour state

        Arguments:
            state {dict} -- The state to calculate the neighbours for

        Returns:
            dict -- The neighbour state
        """

        # There are 2 neighbour generation function. This chooses one of them randomly
        # with an equal probability for each one and then uses the choosen function
        neigh_gen_function = random.choice(
            [
                SimulatedAnnealing.neighbour_generator1,
                SimulatedAnnealing.neighbour_generator2,
            ]
        )
        neighbour_state = neigh_gen_function(state)
        return neighbour_state

class Base:
    """
    Base class for flow scheduling algorithms
    """

    def __init__(self, max_iterations):
        """
        Initialize the Algorithm

        Arguments:
            demand_matrix {np.ndarray} -- The demand matrix created by the demand estimation
        """
        self.max_iterations = max_iterations
        self.demand_matrix = None
        self.state = None

    def update_demand_matrix(self, new_demand_matrix):
        """
        Sets a new demand matrix and updates the routes

        Arguments:
            new_demand_matrix {np.ndarray} -- The demand matrix created by the demand estimation
        """
        self.demand_matrix = new_demand_matrix
        self.state = self.calculate_routes()

    def calculate_routes(self):
        """
        Creates the routes. Needs to be implemented by the actual algorithm.

        Returns:
            dict -- The host to core mapping created by the algorithm
        """
        raise NotImplementedError()

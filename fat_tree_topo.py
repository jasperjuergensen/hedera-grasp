#!/usr/bin/python

from functools import partial

from mininet.net import Mininet
from mininet.log import info, setLogLevel
from mininet.node import RemoteController
from mininet.topo import Topo
from mininet.cli import CLI
from mininet.link import TCLink


class FatTreeTopo(Topo):
    """Fat Tree Topology"""

    def __init__(self, k, bw_c2a=100, bw_a2e=10, bw_e2h=10):
        self.k = k
        self.arp_map = dict()
        self.bw_c2a = bw_c2a
        self.bw_a2e = bw_a2e
        self.bw_e2h = bw_e2h
        super(FatTreeTopo, self).__init__()

    def build(self):
        # create all core switches
        core_switches = []
        for core_switch_id in range(int((self.k / 2) ** 2)):
            # core switches are organized in blocks
            # all core switches in a block are connected to the same
            # aggregation switches
            block_id = core_switch_id // int(self.k / 2) + 1
            in_bock_id = core_switch_id % int(self.k / 2) + 1
            core_switches.append(
                self.addSwitch(
                    "sc-{}".format(core_switch_id),
                    ip="10.{}.{}.{}".format(self.k, block_id, in_bock_id),
                    mac="00:00:00:{:02x}:{:02x}:{:02x}".format(
                        self.k, block_id, in_bock_id
                    ),
                    dpid="0100000000{:02x}{:02x}{:02x}".format(
                        self.k, block_id, in_bock_id
                    ),
                )
            )

        core_downwards_flow_entries = list()

        # for each pod
        for pod_id in range(self.k):

            # add flow table entry for this pod to the core switch
            core_downwards_flow_entries.append(
                "ip,ip_dst=10.{}.{}.{}/16,priority=65535,action={}".format(
                    pod_id, 0, 0, pod_id + 1
                )
            )

            # create the aggregation switches
            agg_switches = []
            for agg_switch_id in range(
                int(self.k / 2), int(self.k / 2) + int(self.k / 2)
            ):
                agg_switches.append(
                    self.addSwitch(
                        "sa-{}-{}".format(pod_id, agg_switch_id),
                        ip="10.{}.{}.{}".format(pod_id, agg_switch_id, 1),
                        mac="00:00:00:{:02x}:{:02x}:{:02x}".format(
                            pod_id, agg_switch_id, 1
                        ),
                        dpid="0200000000{:02x}{:02x}{:02x}".format(
                            pod_id, agg_switch_id, 1
                        ),
                    )
                )
                # connect each aggregation switch to all core switches in its block
                # the block is identified by the id of the agg switch in this pod
                for i in range(int(self.k / 2)):
                    self.addLink(
                        agg_switches[agg_switch_id - int(self.k / 2)],
                        core_switches[
                            (agg_switch_id - int(self.k / 2)) * int(self.k / 2) + i
                        ],
                        cls=TCLink,
                        bw=self.bw_c2a,
                    )

            # create the edge switches
            edge_switches = []
            for edge_switch_id in range(int(self.k / 2)):
                edge_switches.append(
                    self.addSwitch(
                        "se-{}-{}".format(pod_id, edge_switch_id),
                        ip="10.{}.{}.{}".format(pod_id, edge_switch_id, 1),
                        mac="00:00:00:{:02x}:{:02x}:{:02x}".format(
                            pod_id, edge_switch_id, 1
                        ),
                        dpid="0300000000{:02x}{:02x}{:02x}".format(
                            pod_id, edge_switch_id, 1
                        ),
                    )
                )
                # connect each edge switch to all aggregation switches in its pod
                for i in range(int(self.k / 2)):
                    self.addLink(
                        edge_switches[edge_switch_id],
                        agg_switches[i],
                        cls=TCLink,
                        bw=self.bw_a2e,
                    )

                # create the hosts
                hosts = []
                for server_id in range(2, 2 + int(self.k / 2)):
                    hosts.append(
                        self.addHost(
                            "h{:02d}{:02d}{:02d}".format(
                                pod_id, edge_switch_id, server_id
                            ),
                            ip="10.{}.{}.{}".format(pod_id, edge_switch_id, server_id),
                            mac="00:00:00:{:02x}:{:02x}:{:02x}".format(
                                pod_id, edge_switch_id, server_id
                            ),
                        )
                    )
                    # each host is only connected to its edge switch
                    self.addLink(
                        hosts[server_id - 2],
                        edge_switches[edge_switch_id],
                        cls=TCLink,
                        bw=self.bw_e2h,
                    )
                    # save the ip/mac mapping
                    self.arp_map[
                        "10.{}.{}.{}".format(pod_id, edge_switch_id, server_id)
                    ] = "00:00:00:{:02x}:{:02x}:{:02x}".format(
                        pod_id, edge_switch_id, server_id
                    )

        with open("core_switches_downwards_flow_entries", "w+") as f:
            f.write("\n".join(core_downwards_flow_entries))


def install_network(K=4):
    setLogLevel("info")
    topo = FatTreeTopo(K)
    net = Mininet(
        topo=topo,
        link=TCLink,
        controller=partial(RemoteController, ip="127.0.0.1", port=6653),
    )

    # disable IPv6
    for h in net.hosts:
        h.cmd("sysctl -w net.ipv6.conf.all.disable_ipv6=1")
        h.cmd("sysctl -w net.ipv6.conf.default.disable_ipv6=1")
        h.cmd("sysctl -w net.ipv6.conf.lo.disable_ipv6=1")
    for sw in net.switches:
        sw.cmd("sysctl -w net.ipv6.conf.all.disable_ipv6=1")
        sw.cmd("sysctl -w net.ipv6.conf.default.disable_ipv6=1")
        sw.cmd("sysctl -w net.ipv6.conf.lo.disable_ipv6=1")

    net.start()

    # prepare nodes
    # hosts get static arp entries
    # switches get flow table entries for downwards connections
    for hostname in net:
        if hostname.startswith("h"):
            # is server
            host = net.get(hostname)
            for ip, mac in topo.arp_map.items():
                # for all arp mappings in topology
                if ip == host.IP():
                    # don't add arp entry for itself
                    continue
                # add static arp entry
                host.cmd("arp -s {} {}".format(ip, mac))
        elif hostname.startswith("sc"):
            # is core switch
            host = net.get(hostname)
            # add core switch downwards flow table entries
            host.cmd(
                "ovs-ofctl add-flows {} -O OpenFlow13 core_switches_downwards_flow_entries".format(
                    hostname
                )
            )
            host.cmd(
                "ovs-ofctl add-flow {} -O OpenFlow13 priority=0,action=controller".format(
                    hostname
                )
            )
            host.cmd("ovs-vsctl set bridge {} protocols=OpenFlow13".format(hostname))
        elif hostname.startswith("sa"):
            # is aggregation switch
            host = net.get(hostname)
            pod_id = int(hostname.split("-")[1])
            for edge_switch_id in range(int(K / 2)):
                # for each edge switch connected to this aggregation switch
                # create flow table entry for edge switch network
                host.cmd(
                    "ovs-ofctl add-flow {} -O OpenFlow13 ip,ip_dst=10.{}.{}.{}/24,priority=65535,action={}".format(
                        hostname,
                        pod_id,
                        edge_switch_id,
                        0,
                        int(K / 2) + edge_switch_id + 1,
                    )
                )
            host.cmd(
                "ovs-ofctl add-flow {} -O OpenFlow13 priority=0,action=controller".format(
                    hostname
                )
            )
            host.cmd("ovs-vsctl set bridge {} protocols=OpenFlow13".format(hostname))
        elif hostname.startswith("se"):
            # is edge switch
            host = net.get(hostname)
            _, pod_id, edge_switch_id = hostname.split("-")
            for server_id in range(2, 2 + int(K / 2)):
                # for each host connected to the edge switch
                # create flow table entry for host
                # edge switches works on layer 2
                host.cmd(
                    "ovs-ofctl add-flow {} -O OpenFlow13 eth_dst=00:00:00:{:02x}:{:02x}:{:02x},priority=65535,action={}".format(
                        hostname,
                        int(pod_id),
                        int(edge_switch_id),
                        server_id,
                        int(K / 2) + server_id - 2 + 1,
                    )
                )
            host.cmd(
                "ovs-ofctl add-flow {} -O OpenFlow13 priority=0,action=controller".format(
                    hostname
                )
            )
            host.cmd("ovs-vsctl set bridge {} protocols=OpenFlow13".format(hostname))
    return net


def main():
    net = install_network()
    CLI(net)
    net.stop()


if __name__ == "__main__":
    main()

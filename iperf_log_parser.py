import os, sys
import pandas as pd


def logfile_to_df(filename):
    """
    Creates a pandas dataframe from a iperf csv log file

    Arguments:
        filename {str} -- name / path of the log file to read

    Return:
        dataframe {pandas.DataFrame} -- dataframe with a row per transfer and col (src_ip, src_port, dst_ip, dst_port, bits_per_second)
    """

    # read csv from iperf output mit option -y C into df
    i_df = pd.read_csv(
        filename,
        header=None,
        names=[
            "timestamp",
            "src_ip",
            "src_port",
            "dst_ip",
            "dst_port",
            "transfer_id",
            "interval",
            "transferred_bytes",
            "bits_per_second",
        ],
    )

    if len(i_df.index) == 0:
        return None

    return (
        i_df[
            [
                "src_ip",
                "src_port",
                "dst_ip",
                "dst_port",
                "transfer_id",
                "bits_per_second",
            ]
        ]
        .groupby(["transfer_id", "src_ip", "src_port", "dst_ip", "dst_port"])
        .apply(lambda group: group.iloc[1:-1])
        .reset_index(drop=True)
        .groupby(["transfer_id", "src_ip", "src_port", "dst_ip", "dst_port"])
        .mean()
        .reset_index()
    )


def load_all_logs_from_dir(dirname):
    """
    Loads all logfiles from a given directory

    Arguments:
        dirname {str} -- path to the directory

    Return:
        pandas.Dataframe -- dataframe containing data from all the logfiles
    """

    df_list = []
    for filename in os.listdir(dirname):
        if filename.startswith("iperf_") and filename.endswith(".csv"):
            parsed_log = logfile_to_df(os.path.join(dirname, filename))
            if parsed_log is not None:
                df_list.append(parsed_log)

    return pd.concat(df_list, ignore_index=True)


def filter_df_bisection(df, k=4):
    """
    Filters df to only flows crossing the middle of a fattreetopo(k)

    Arguments:
        df {pandas.DataFrame} -- dataframe containing all data the logfiles
        k=4 {int} -- defining the fattreetopo size, where to split

    Return:
        pandas.Dataframe -- dataframe containing only data from flows crossing the middle
    """

    k2 = k / 2
    return df.loc[
        ((df.src_ip < "10.{}.0.0".format(k2)) & (df.dst_ip > "10.{}.0.0".format(k2)))
        | ((df.src_ip > "10.{}.0.0".format(k2)) & (df.dst_ip < "10.{}.0.0".format(k2)))
    ]


def df_get_mean(df):
    """
    Returns the mean bits_per_second

    Arguments:
        df {pandas.DataFrame} -- dataframe with from where the mean should be taken

    Return:
        float -- mean bits per second
    """
    return df.mean()["bits_per_second"]


def df_get_median(df):
    """
    Returns the median bits_per_second

    Arguments:
        df {pandas.DataFrame} -- dataframe with from where the median should be taken

    Return:
        float -- median bits per second
    """
    return df.median()["bits_per_second"]


def df_get_bisection_sum(df):
    """
    Returns the sum of bits_per_second of the bisection of the given dataframe

    Arguments:
        df {pandas.DataFrame} -- dataframe which should be used to take a bisection and sum the bits per second

    Return:
        float -- sum of the bits per second over the bisection
    """
    return filter_df_bisection(df).sum()["bits_per_second"]


def main():
    """
    Parses the logfiles in the given directory and prints the statistics
    """

    # working directory is default logfile directory
    log_dir = "."

    # if argument given, use it as logfile directory
    if len(sys.argv) > 1:
        log_dir = sys.argv[1]

    # load all logs from directory
    df = load_all_logs_from_dir(log_dir)

    # print statistics
    print ("Throughput mean, Throughput median, Throughput bisection sum")
    print (
        "{}, {}, {}".format(
            df_get_mean(df), df_get_median(df), df_get_bisection_sum(df)
        )
    )


if __name__ == "__main__":
    main()

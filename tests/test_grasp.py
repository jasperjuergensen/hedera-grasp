import random
import sys

import numpy as np

from algorithms.grasp import GRASP
from config import CONF
from utils.demand_estimation import demand_estimation

CONF(sys.argv[1:])

M = np.zeros((CONF.host_count, CONF.host_count, 3))

for _ in range(30):
    # create random flows
    src = random.randint(0, CONF.host_count - 1)
    dst = random.randint(0, CONF.host_count - 1)
    while src == dst:
        # source and dest cannot be the same
        src = random.randint(0, CONF.host_count - 1)
        dst = random.randint(0, CONF.host_count - 1)
    M[src, dst, 0] += 1

print (M[:, :, 0])

# run demand estimation
demand_estimation(M)

algo = GRASP(100)

# after initialization there is no state
assert algo.state is None

algo.update_demand_matrix(M[:, :, 1] * M[:, :, 0])

# after an update the state changed
print (algo.state)

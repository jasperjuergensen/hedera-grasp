#!/usr/bin/python
import os

from utils.create_params import create_params

create_params("params.conf", 4)

os.execvp("ryu-manager", ["ryu-manager", "switch.py", "--config-file", "params.conf"])

#!/usr/bin/python

from fat_tree_topo import FatTreeTopo
from utils.get_topo import mininet_topo_to_nx
from utils.plot_nx_graph import plot_nx_graph

miniTopo = FatTreeTopo(4)

G = mininet_topo_to_nx(miniTopo)

plot_nx_graph(G)

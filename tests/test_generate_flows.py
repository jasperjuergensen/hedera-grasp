import numpy as np

from config import CONF
from utils.generate_flows import (
    generate_stride,
    generate_staggered,
    generate_random,
    generate_random_bijektive,
)

CONF(["--config-file", "params.conf"])

HOST_COUNT = CONF.host_count


def test_generate_stride():
    M = generate_stride(HOST_COUNT, 2)
    assert M.sum() == HOST_COUNT
    assert np.diag(M).sum() == 0
    assert M[0][2] == 1
    assert M[1][3] == 1
    assert (M.sum(axis=1) == np.array([1] * HOST_COUNT)).all()
    assert (M.sum(axis=0) == np.array([1] * HOST_COUNT)).all()


def test_generate_stride_zero():
    try:
        generate_stride(HOST_COUNT, 0)
        assert False
    except ValueError:
        assert True


def test_generate_stride_zero_diag():
    M = generate_stride(HOST_COUNT, 1)
    assert np.diag(M).sum() == 0


def test_generate_staggered_parameter_failed():
    # test edgeP greater 1
    try:
        generate_staggered(HOST_COUNT, 2, 0)
        assert False
    except ValueError:
        assert True

    # test edgeP lower 0
    try:
        generate_staggered(HOST_COUNT, -0.3, 0)
        assert False
    except ValueError:
        assert True

    # test podP greater 1
    try:
        generate_staggered(HOST_COUNT, 0, 2)
        assert False
    except ValueError:
        assert True

    # test podP lower 0
    try:
        generate_staggered(HOST_COUNT, 0, -0.3)
        assert False
    except ValueError:
        assert True

    # test sum greater 1
    try:
        generate_staggered(HOST_COUNT, 0.5, 0.7)
        assert False
    except ValueError:
        assert True

    # test number_of_flows greater host_count
    try:
        generate_staggered(HOST_COUNT, 0.1, 0.1, number_of_flows=HOST_COUNT + 2)
        assert False
    except ValueError:
        assert True

    # test number_of_flows lower than 1
    try:
        generate_staggered(HOST_COUNT, 0.1, 0.1, number_of_flows=0)
        assert False
    except ValueError:
        assert True
    try:
        generate_staggered(HOST_COUNT, 0.1, 0.1, number_of_flows=-2)
        assert False
    except ValueError:
        assert True


def test_generate_staggered():
    M = generate_staggered(HOST_COUNT, 0.1, 0.2)
    assert M.sum() == HOST_COUNT

    # only one flow per source
    assert (M.sum(axis=1) == np.array([1] * HOST_COUNT)).all()


def test_generate_staggered_probs_zero():
    # because random is involved run this test 10 times
    for x in range(10):
        M = generate_staggered(HOST_COUNT, 0, 0)
        # there should be zero connections within a pod
        for pod_id in range(CONF.pod_count):
            assert (
                M[
                    pod_id * CONF.pod_size : (pod_id + 1) * CONF.pod_size,
                    pod_id * CONF.pod_size : (pod_id + 1) * CONF.pod_size,
                ].sum()
                == 0
            )


def test_generate_staggered_pod_prop_one():
    M = generate_staggered(HOST_COUNT, 0, 1)
    assert M.sum() == HOST_COUNT
    flow_sum = 0
    # all flows should be within a pod
    print (M)
    for pod_id in range(CONF.pod_count):
        flow_sum += M[
            pod_id * CONF.pod_size : (pod_id + 1) * CONF.pod_size,
            pod_id * CONF.pod_size : (pod_id + 1) * CONF.pod_size,
        ].sum()
    assert flow_sum == HOST_COUNT


def test_generate_staggered_number_of_flows():
    # test this for all valid values
    for i in range(1, HOST_COUNT):
        M = generate_staggered(HOST_COUNT, 0.1, 0.2, number_of_flows=i)
        assert M.sum() == i


def test_generate_staggered_zero_diag():
    M = generate_staggered(HOST_COUNT, 0.5, 0.3)
    assert np.diag(M).sum() == 0


def test_generate_random():
    M = generate_random(HOST_COUNT)
    assert M.sum() == HOST_COUNT

    # only one flow per source
    assert (M.sum(axis=1) == np.array([1] * HOST_COUNT)).all()


def test_generate_random_number_of_flows():
    # test this for all valid values
    for i in range(1, HOST_COUNT):
        M = generate_random(HOST_COUNT, number_of_flows=i)
        assert M.sum() == i


def test_generate_random_zero_diag():
    M = generate_random(HOST_COUNT)
    assert np.diag(M).sum() == 0


def test_generate_random_bijektive():
    M = generate_random_bijektive(HOST_COUNT)
    # for all src, dst pairs check if src->dst and dst->src are the same
    for src_host_id in range(HOST_COUNT):
        for dst_host_id in range(HOST_COUNT):
            assert M[src_host_id, dst_host_id] == M[dst_host_id, src_host_id]


def test_generate_random_biijektive_zero_diag():
    M = generate_random_bijektive(HOST_COUNT)
    assert np.diag(M).sum() == 0

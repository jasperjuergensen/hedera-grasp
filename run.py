import os
import subprocess
import sys
from time import sleep

from config import CONF
from fat_tree_topo import install_network
from logger import logger
from utils.create_params import get_flat_host_list, get_host_count
from utils.generate_flows import (
    generate_stride,
    generate_staggered,
    generate_random,
    generate_random_bijektive,
)

# load config
CONF(sys.argv[1:])


TESTS = {
    "stride": generate_stride,
    "staggered": generate_staggered,
    "random": generate_random,
    "random_bijektive": generate_random_bijektive,
}


def send_data(net, flows_to_send, flat_host_list, seconds):
    """
    Start iperf measurements.

    Arguments:
        net -- The mininet instance
        flows_to_send {np.ndarray} -- A matrix with src as row and dst as column and the number of flows to send as values
        flat_host_list {List[str]} -- A list with all hostnames
        seconds {int} -- The number of seconds to run the tests for
    """
    # On all hosts start an iperf server
    for host in flat_host_list:
        net.getNodeByName(host).cmd(
            "iperf -s -p 5001 -i 10 -yc > logging/server/iperf_{}.csv &".format(host)
        )

    # start iperf clients
    for src_host_id in range(len(flat_host_list)):
        for dst_host_id in range(len(flat_host_list)):
            for flow_id in range(flows_to_send[src_host_id, dst_host_id]):
                dst_hostname = flat_host_list[dst_host_id]
                src_hostname = flat_host_list[src_host_id]
                logger.info("Starting iperf from %s to %s", src_hostname, dst_hostname)
                dst_ip = "10.{}.{}.{}".format(dst_hostname[1:3], dst_hostname[3:5], dst_hostname[5:7])
                net.getNodeByName(src_hostname).cmd(
                    "iperf -c {} -p 5001 -L {} -t {} > logging/client/client_{}.log 2>&1 &".format(
                        dst_ip, 5002 + flow_id, seconds, src_hostname
                    )
                )

    for i in range(0, seconds, 10):
        logger.info("%d s elapsed", i)
        sleep(10)

    # kill all iperfs
    os.system("kill -9 $(pidof iperf)")


def create_folders():
    """
    Create folders for logging
    """
    if not os.path.isdir("logging"):
        os.mkdir("logging")
    if not os.path.isdir("logging/client"):
        os.mkdir("logging/client")
    if not os.path.isdir("logging/server"):
        os.mkdir("logging/server")


def main(k=4):
    create_folders()
    flat_host_list = get_flat_host_list(k)
    host_count = get_host_count(k)
    # start ryu controller
    ryu_controller = subprocess.Popen(
        ["ryu-manager", "switch.py", "--config-file", "params.conf"]
    )
    try:
        sleep(2)
        # install mininet fat tree topo network
        net = install_network(k)

        try:
            if CONF.test not in TESTS:
                raise Exception("Invalid test type {}".format(CONF.test))

            test = TESTS.get(CONF.test)
            test_params = CONF.test_params

            flows_to_send = test(host_count, **test_params)
            # flows_to_send = generate_stride(host_count, 1)
            # flows_to_send = generate_staggered(host_count, 0.2, 0.3)
            # flows_to_send = generate_random(host_count)
            # flows_to_send = generate_random_bijektive(host_count)

            # send predefined flows
            send_data(net, flows_to_send, flat_host_list, 60)
        except Exception as e:
            logger.exception("Error after starting the network")
        finally:
            # shutdown network
            net.stop()
    except Exception as e:
        logger.exception("Error after starting the ryu controller")
    finally:
        # terminate controller
        ryu_controller.terminate()


if __name__ == "__main__":
    main()

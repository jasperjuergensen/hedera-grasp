import array
import hashlib

from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import MAIN_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.lib.packet import packet, ipv4, udp, tcp, icmp, in_proto
from ryu.ofproto import ofproto_v1_3

from config import CONF
from logger import logger


class ECMPApplication(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    def __init__(self, *args, **kwargs):
        super(ECMPApplication, self).__init__(*args, **kwargs)

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def packet_in_handler(self, ev):
        msg = ev.msg
        dp = msg.datapath
        ofp = dp.ofproto
        ofp_parser = dp.ofproto_parser

        # parse the packet
        pkt = packet.Packet(array.array("B", msg.data))
        ip_packet = pkt.get_protocol(ipv4.ipv4)
        if ip_packet is None:
            logger.warning("Unsupported L3 Protocol")
            return
        if ip_packet.proto == in_proto.IPPROTO_ICMP:
            l4_proto = pkt.get_protocol(icmp.icmp)
        elif ip_packet.proto == in_proto.IPPROTO_TCP:
            l4_proto = pkt.get_protocol(tcp.tcp)
        elif ip_packet.proto == in_proto.IPPROTO_UDP:
            l4_proto = pkt.get_protocol(udp.udp)
        else:
            logger.warning("Unsupported L4 protocol")
            return

        # create hash
        m = hashlib.md5()
        m.update(ip_packet.src.encode("ascii"))
        m.update(ip_packet.dst.encode("ascii"))
        m.update(str(ip_packet.proto).encode("ascii"))
        if ip_packet.proto in [in_proto.IPPROTO_TCP, in_proto.IPPROTO_UDP]:
            m.update(str(l4_proto.src_port).encode("ascii"))
            m.update(str(l4_proto.dst_port).encode("ascii"))
        elif ip_packet.proto == in_proto.IPPROTO_ICMP:
            m.update(str(l4_proto.type).encode("ascii"))
            m.update(str(l4_proto.code).encode("ascii"))
        else:
            logger.warning("Unsupported L4 protocol")
            return
        h = int(m.hexdigest(), 16)

        # calc output port using hash modulo number of output ports
        out_port = (h % CONF.pod_edge_count) + 1

        # create an output action for the output port
        actions = [ofp_parser.OFPActionOutput(out_port)]

        # send an packet out message with the output port to the controller
        out = ofp_parser.OFPPacketOut(
            datapath=dp,
            buffer_id=msg.buffer_id,
            in_port=ofp.OFPP_CONTROLLER,
            actions=actions,
        )
        dp.send_msg(out)

        # match on the flow (exact)
        if ip_packet.proto == in_proto.IPPROTO_TCP:
            match = ofp_parser.OFPMatch(
                eth_type=0x0800,
                ipv4_src=ip_packet.src,
                ipv4_dst=ip_packet.dst,
                ip_proto=in_proto.IPPROTO_TCP,
                tcp_src=l4_proto.src_port,
                tcp_dst=l4_proto.dst_port,
            )
        elif ip_packet.proto == in_proto.IPPROTO_UDP:
            match = ofp_parser.OFPMatch(
                eth_type=0x0800,
                ipv4_src=ip_packet.src,
                ipv4_dst=ip_packet.dst,
                ip_proto=in_proto.IPPROTO_UDP,
                udp_src=l4_proto.src_port,
                udp_dst=l4_proto.dst_port,
            )
        elif ip_packet.proto == in_proto.IPPROTO_ICMP:
            match = ofp_parser.OFPMatch(
                eth_type=0x0800,
                ipv4_src=ip_packet.src,
                ipv4_dst=ip_packet.dst,
                ip_proto=in_proto.IPPROTO_ICMP,
                icmpv4_type=l4_proto.type,
                icmpv4_code=l4_proto.code,
            )
        else:
            logger.warning("Unsupported L4 Proto")
            return

        # create instruction for the action
        inst = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS, actions)]

        # create a rule to output all matched packets on the output interface
        rule = ofp_parser.OFPFlowMod(
            datapath=dp,
            match=match,
            buffer_id=msg.buffer_id,
            instructions=inst,
            idle_timeout=120,
            priority=100,
        )
        dp.send_msg(rule)

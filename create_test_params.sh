#!/bin/bash

set -e

for algo in {"annealing","grasp","none"}; do
    sed "s/^algorithm = annealing/algorithm = ${algo}/" params.conf > "params_${algo}.conf"

    for params in {"i:1","i:2","i:4","i:8"}; do
        sed "s/^test_params = i:1$/test_params = ${params}/" "params_${algo}.conf" > "params_${algo}_stride_${params}.conf"
    done

    sed "s/^test = stride$/test = staggered/" "params_${algo}.conf" > "params_${algo}_staggered.conf"

    for params in {"edgeP:0.2,podP:0.3","edgeP:0.5,podP:0.3"}; do
        for seed in 7835 8501 5567 9414 2470 5440 5181 455 5248 7362; do
            seed_params="${params},seed:${seed}"
            sed "s/^test_params = i:1$/test_params = ${seed_params}/" "params_${algo}_staggered.conf" > "params_${algo}_staggered_${seed_params}.conf"
        done
    done

    sed "s/^test = stride$/test = random/" "params_${algo}.conf" > "params_${algo}_random.conf"
    for seed in 7835 8501 5567 9414 2470 5440 5181 455 5248 7362; do
        params="seed:${seed}"
        sed "s/^test_params = i:1$/test_params = ${params}/" "params_${algo}_random.conf" > "params_${algo}_random_${params}.conf"
    done

    sed "s/^test = stride$/test = random_bijektive/" "params_${algo}.conf" > "params_${algo}_random_bijektive.conf"
    for seed in 7835 8501 5567 9414 2470 5440 5181 455 5248 7362; do
        params="seed:${seed}"
        sed "s/^test_params = i:1$/test_params = ${params}/" "params_${algo}_random_bijektive.conf" > "params_${algo}_random_bijektive_${params}.conf"
    done
done

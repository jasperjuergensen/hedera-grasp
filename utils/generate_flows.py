import random

import numpy as np

from config import CONF


def generate_stride(host_count, i):
    """
    Generate a stride flow pattern.
    A host with index x sends to the host with index (x + i) mode (num_hosts). This is done for all hosts.

    Arguments:
        host_count {int} -- The number of hosts in the network
        i {int} -- The shift parameter for stride. Must be greater than 0.

    Returns:
        np.ndarray -- A matrix with src as rows, dst as columns and number of flows as values
    """
    # check i param
    i = int(i)
    if i < 1:
        raise ValueError("i must be greater than 0")

    # build flow matrix with 1 in the diagonal
    flow_matrix = np.eye(host_count, dtype=np.int)

    # roll the ones from the diag i times to the right
    flow_matrix = np.roll(flow_matrix, i, axis=1)
    return flow_matrix


def generate_staggered(host_count, edgeP, podP, seed=None, number_of_flows=None):
    """
    Generate a staggered prob flow pattern.
    A host sends to another host in the same edge with probability edgeP and to its same pod
    with probability podP and to the rest of the network with probability 1 - edgeP - podP.

    Arguments:
        host_count {int} -- The number of hosts in the network
        edgeP {float} -- The probability to send to a host in the same edge
        podP {float} -- The probability to send to a host in the same pod
        seed {int} -- Seed for the random generator
        number_of_flows {int} -- How many flows to generate. Defaults to the number of hosts.
            Must be between 1 and host_count

    Returns:
        np.ndarray -- A matrix with src as rows, dst as columns and number of flows as values
    """
    # check number of flows param
    if number_of_flows is None:
        number_of_flows = host_count
    number_of_flows = int(number_of_flows)
    if number_of_flows <= 0:
        raise ValueError("Number of flows must be greater than 0")
    if number_of_flows > host_count:
        raise ValueError("Number of flows cannot be greater than host_count")

    # check edgeP param
    edgeP = float(edgeP)
    if edgeP < 0:
        raise ValueError("Edge probability cannot be lower than 0")
    if edgeP > 1:
        raise ValueError("Edge probability cannot be larger than 1")

    # check podP param
    podP = float(podP)
    if podP < 0:
        raise ValueError("Pod probability cannot be lower than 0")
    if podP > 1:
        raise ValueError("Pod probability cannot be larger than 1")

    # check sum of edgeP and podP param
    if edgeP + podP > 1:
        raise ValueError("Sum of edge and pod probability cannot be larger than 1")

    if seed is not None:
        random.seed(int(seed))

    # build empty flow matrix
    flow_matrix = np.zeros((host_count, host_count), dtype=np.int)
    # make a list of all host ids
    hosts = list(range(host_count))

    for x in range(number_of_flows):
        # get a random host
        host_id = random.choice(hosts)

        # every host can be selected only once, so delete it from the host list
        del hosts[hosts.index(host_id)]

        # get the flow type (edge, pod, other)
        flow_type = random.random()

        if flow_type < edgeP:
            # in same edge
            edge_id = host_id // (CONF.pod_count // 2)
            other_host_id = random.randrange(
                edge_id * (CONF.pod_count / 2), ((edge_id + 1) * (CONF.pod_count / 2))
            )
            while other_host_id == host_id:
                # prevent flows to itself
                other_host_id = random.randrange(
                    edge_id * (CONF.pod_count / 2), (edge_id + 1) * (CONF.pod_count / 2)
                )
        elif flow_type < podP:
            # in same pod
            pod_id = host_id // CONF.pod_count
            other_host_id = random.randrange(
                pod_id * CONF.pod_size, (pod_id + 1) * CONF.pod_count
            )
            while other_host_id == host_id:
                # prevent flows to itself
                other_host_id = random.randrange(
                    pod_id * CONF.pod_size, (pod_id + 1) * CONF.pod_count
                )
        else:
            # rest of the network
            pod_id = host_id // CONF.pod_count
            other_host_id = random.randint(0, host_count - 1)
            other_pod_id = other_host_id // CONF.pod_count
            while other_pod_id == pod_id:
                # prevent hosts in the same pod (and therefor in the same edge)
                other_host_id = random.randint(0, host_count - 1)
                other_pod_id = other_host_id // CONF.pod_count
        flow_matrix[host_id, other_host_id] += 1
    return flow_matrix


def generate_random(host_count, seed=None, number_of_flows=None):
    """
    Generate a random flow pattern.
    A host sends to any other host in the network with uniform probability.

    Arguments:
        host_count {int} -- The number of hosts in the network
        seed {int} -- Seed for the random generator
        number_of_flows {int} -- How many flows to generate. Defaults to the number of hosts.
            Must be between 1 and host_count

    Returns:
        np.ndarray -- A matrix with src as rows, dst as columns and number of flows as values
    """
    # check number of flows param
    if number_of_flows is None:
        number_of_flows = host_count
    number_of_flows = int(number_of_flows)
    if number_of_flows <= 0:
        raise ValueError("Number of flows must be greater than 0")
    if number_of_flows > host_count:
        raise ValueError("Number of flows cannot be greater than host_count")

    if seed is not None:
        random.seed(int(seed))

    # build empty flow matrix
    flow_matrix = np.zeros((host_count, host_count), dtype=np.int)
    # make a list of all host ids
    hosts = list(range(host_count))

    for x in range(number_of_flows):
        # get a random host
        host_id = random.choice(hosts)

        # every host can be selected only once, so delete it from the host list
        del hosts[hosts.index(host_id)]

        # get random other host
        other_host_id = random.randrange(0, host_count)
        while other_host_id == host_id:
            # prevent flows to itself
            other_host_id = random.randrange(0, host_count)
        flow_matrix[host_id, other_host_id] += 1

    return flow_matrix


def generate_random_bijektive(host_count, seed=None, number_of_flows=None):
    """
    Generate a random bijective mapping flow pattern
    A host sends to any other host in the network with uniform probability.
    There is also for every flow a flow in the opposite direction.

    Arguments:
        host_count {int} -- The number of hosts in the network
        seed {int} -- Seed for the random generator
        number_of_flows {int} -- How many flows to generate in the random phase.
            In the end there will be this number doubled flows.
            Defaults to the number of hosts. Must be between 1 and host_count.

    Returns:
        np.ndarray -- A matrix with src as rows, dst as columns and number of flows as values
    """
    # get random flow matrix
    flow_matrix = generate_random(host_count, seed, number_of_flows)

    # add the transposed flow matrix (src and dst exchanged)
    flow_matrix += flow_matrix.T
    return flow_matrix

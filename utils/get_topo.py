#!/bin/python

import networkx as nx
import pickle
import mininet

from mininet.node import RemoteController


def mininet_topo_to_nx(topology):
    # G = nx.MultiGraph()
    G = topology.convertTo(cls=nx.MultiGraph)
    return G


def serialize(G, filename):
    pickle.dump(G, open(filename, "wb"))


def deserialize(filename):
    G = pickle.load(open(filename, "rb"))
    return G

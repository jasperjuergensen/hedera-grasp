import numpy as np

from logger import logger
from .demand_estimation import demand_estimation
from config import CONF

# https://ryu.readthedocs.io/en/latest/ofproto_v1_2_ref.html#ryu.ofproto.ofproto_v1_2_parser.OFPFlowStats

THRESH = 0.1 * CONF.interface_speed  # 10% of INTERFACE_SPEED


def handle_stats(stats):
    """
    Calculates the demands from the openflow statistics

    Arguments:
        stats {List[ryu.ofproto.ofproto_v1_2_parser.OFPFlowStats]} -- A list of ryu openflow stats objects

    Returns:
        Tuple[np.ndarray, list] -- The demands as calculated by the demand estimation and the elephant flows
    """

    # create empty demand matrix
    M = np.zeros((CONF.host_count, CONF.host_count, 3))

    # create empty list of flows to influence
    flows = []

    for stat in stats:
        # calculate demand as throughput (bytes divided by duration)
        try:
            demand = (stat.byte_count * 8) / (
                stat.duration_sec + (stat.duration_nsec / 1e9)
            )
        except ZeroDivisionError:
            logger.warning("Flow with zero seconds duration found")
            demand = 0

        if demand > THRESH:
            # large flow
            match = stat.match
            src_host = CONF.host_ip_dict[match.get("ipv4_src")]
            dst_host = CONF.host_ip_dict[match.get("ipv4_dst")]

            # add one flow to the flow count for src -> dest
            M[
                CONF.flat_host_list.index(src_host),
                CONF.flat_host_list.index(dst_host),
                0,
            ] += 1

            # add flow to list
            flows.append(stat)

    # run the demand estimation
    demand_estimation(M)

    # return demand matrix but only the demands multiplied by the number of flows
    return (M[:, :, 0] * M[:, :, 1], flows)

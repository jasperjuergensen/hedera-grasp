import math

from fat_tree_topo import FatTreeTopo
from get_topo import mininet_topo_to_nx


def create_params(filename, k=4):
    """
    Create config file with parameters for the SDN application

    Arguments:
        filename {str} -- The filename for the config file
        k {int} -- The k parameter for the fat-tree-topology. Defaults to 4
    """
    topo = FatTreeTopo(k)
    G = mininet_topo_to_nx(topo)

    host_ip_dict = ""
    for key, value in G.nodes().iteritems():
        host_ip_dict += "{}:{},{}:{},".format(key, value["ip"], value["ip"], key)
    host_ip_dict = host_ip_dict[:-1]

    flat_host_list = sorted([host for host in G.nodes() if host.startswith("h")])
    host_count = len(flat_host_list)
    core_list = sorted([core for core in G.nodes() if core.startswith("sc")])
    core_count = len(core_list)
    switch_list = sorted(
        [
            switch
            for switch in G.nodes()
            if switch.startswith("sa") or switch.startswith("se")
        ]
    )
    pod_edge_count = k // 2

    config = {
        "hedera_algorithm": "annealing",
        "iterations": 100,
        "interface_speed": 10000000,
        "flat_host_list": ",".join(flat_host_list),
        "host_count": host_count,
        "core_list": ",".join(core_list),
        "core_count": core_count,
        "switch_list": ",".join(switch_list),
        "pod_count": k,
        "pod_size": (k // 2) * (k // 2),
        "pod_edge_count": pod_edge_count,
        "host_ip_dict": host_ip_dict,
        "test": "stride",
        "test_params": "i:1",
    }

    with open(filename, "w") as conf_file:
        conf_file.write("[DEFAULT]\n")
        for key, value in config.items():
            conf_file.write("{} = {}\n".format(key, value))


def get_flat_host_list(k=4):
    """
    Get the flat host list for a fat-tree-topology

    Arguments:
        k {int} -- The k parameter for the fat-tree-topology. Defaults to 4
    """
    G = mininet_topo_to_nx(FatTreeTopo(k))
    return sorted([host for host in G.nodes() if host.startswith("h")])


def get_host_count(k=4):
    """
    Get the number of hosts for a fat-tree-topology

    Arguments:
        k {int} -- The k parameter for the fat-tree-topology. Defaults to 4
    """
    return len(get_flat_host_list(k))

#!/usr/bin/python

import networkx as nx
import matplotlib.pyplot as plt

# from networkx.drawing.nx_pydot import graphviz_layout


def plot_nx_graph(G):
    # this will draw a nicer tree but needs graphviz installed on the host
    # pos = graphviz_layout(G, prog="dot")
    # nx.draw(G, pos=pos)
    nx.draw(G)
    plt.savefig("nxGraph.png")
    plt.clf()

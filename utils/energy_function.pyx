from config import CONF

import numpy as np

cimport numpy as np

DTYPE = np.float

ctypedef np.float_t DTYPE_t


def energy_function(dict state, np.ndarray demand_matrix):
    """
    Function to calculate how good the current solution is.
    Currently this calculates the total demand on exceeded links (links where the demand is higher then 1).

    Arguments:
        state {dict} -- The state to calculate the energy for
        demand_matrix {np.ndarray} -- The demand matrix as created by the demand estimation

    Returns:
        float -- The energy for the state
    """
    # define variables
    cdef int src_host_id, dst_host_id, core_id, src_pod_id, dst_pod_id, src_edge_group, dst_edge_group, pod_size, pod_count, core_count, pod_edge_count
    cdef DTYPE_t demand

    # copy CONF variables to c variables
    pod_size = CONF.pod_size
    pod_count = CONF.pod_count
    core_count = CONF.core_count
    pod_edge_count = CONF.pod_edge_count

    # create for every link an entry
    # in every pod there are pod_size links and there are pod_count pods
    cdef np.ndarray edge_agg_link_demands =np.zeros([pod_count, pod_edge_count * pod_edge_count], dtype=DTYPE)
    # for every core there are pod_size links to the core
    cdef np.ndarray core_link_demands = np.zeros([core_count * pod_size], dtype=DTYPE)

    for src_host_id in range(CONF.host_count):
        for dst_host_id in range(CONF.host_count):
            demand = demand_matrix[src_host_id, dst_host_id]
            if demand == 0:
                # ignore if no flows between this two hosts
                continue

            # get the used core from the state
            core = state.get(CONF.flat_host_list[dst_host_id])
            if core is None:
                # ignore flows which don't have a core mapped yet (grasp make_rcl)
                continue
            # get the id of the used core
            core_id = int(core.split("-")[1])

            # get the pod ids for the src and the dst host
            src_pod_id = src_host_id // pod_size
            dst_pod_id = dst_host_id // pod_size

            # get the id of the edge router that connects the src and dst host
            src_edge_group = (src_host_id % pod_size) // pod_edge_count
            dst_edge_group = (dst_host_id % pod_size) // pod_edge_count

            if src_pod_id != dst_pod_id:
                # only flows between different pods go over the core
                # add the upwards core demand for the flow from src_pod to core
                core_link_demands[src_pod_id * core_count + core_id] += demand
                # add the downwards core demand for the flow from core to dst_pod
                core_link_demands[dst_pod_id * core_count + core_id] += demand

            # in the pod add the demand to the link that goes to the agg switch that connects to the core
            edge_agg_link_demands[
                src_pod_id,
                core_id // pod_edge_count + src_edge_group * pod_edge_count
            ] += demand
            edge_agg_link_demands[
                dst_pod_id,
                core_id // pod_edge_count + dst_edge_group * pod_edge_count
            ] += demand

    # sum up the demands that are greater than 1
    result = np.sum(edge_agg_link_demands[np.where(edge_agg_link_demands > 1)])
    result += np.sum(core_link_demands[np.where(core_link_demands > 1)])

    return result

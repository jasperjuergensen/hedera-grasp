from utils.statistics.confidence_interval import confidence_interval


def means_of_difference(X1, X2, alpha):
    """
    Checks if there is a statistically significant difference between X1 and X1 for a confidence level of alpha
    using the means of difference method. Both alternatives must have the same number of measurements.

    Arguments:
        X1 {List[float]} -- List of measurements for alternative 1
        X2 {List[float]} -- List of measurements for alternative 2
        alpha {float} -- Confidence level

    Returns:
        bool -- True if there is a statistically significant difference
    """
    if len(X1) != len(X2):
        raise ValueError("both alternatives must have the same number of measurements")

    differences = [x1 - x2 for x1, x2 in zip(X1, X2)]

    c1, c2 = confidence_interval(differences, alpha)
    return not c1 < 0 < c2

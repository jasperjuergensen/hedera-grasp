import statistics
from math import sqrt

from utils.statistics.confidence_interval import confidence_interval_


def difference_of_means(X1, X2, alpha):
    """
    Checks if there is a statistically significant difference between X1 and X1 for a confidence level of alpha
    using the difference of means method

    Arguments:
        X1 {List[float]} -- List of measurements for alternative 1
        X2 {List[float]} -- List of measurements for alternative 2
        alpha {float} -- Confidence level

    Returns:
        bool -- True if there is a statistically significant difference
    """
    mean1 = statistics.mean(X1)
    mean2 = statistics.mean(X2)
    n1 = len(X1)
    n2 = len(X2)
    stdev1 = statistics.stdev(X1)
    stdev2 = statistics.stdev(X2)
    difference = mean1 - mean2
    diff_stdev = sqrt(((stdev1 ** 2) / n1) + ((stdev2 ** 2) / n2))
    df_difference = ((((stdev1 ** 2) / n1) + ((stdev2 ** 2) / n2)) ** 2) / (
        ((((stdev1 ** 2) / n1) ** 2) / (n1 - 1))
        + ((((stdev2 ** 2) / n2) ** 2) / (n2 - 1))
    )
    c1, c2 = confidence_interval_(df_difference, difference, diff_stdev, alpha)
    return not c1 < 0 < c2

from math import sqrt, ceil

import scipy.stats as stats
import statistics


def confidence_interval_(n, mean, stdev, alpha):
    """
    Calculates a confidence interval

    Arguments:
        n {int} -- Number of measurements or degree of freedom + 1
        mean {float} -- Mean of the measurements
        stdev {float} -- Standard deviation of the measurements
        alpha {float} -- The confidence level

    Returns:
        Tuple[float, float] --  A tuple of two elements. The first one is the lower bound, the second one the upper bound
    """
    if alpha > 1 or alpha < 0:
        raise ValueError("alpha must be between 0 and 1")

    if n >= 30:
        t_value = stats.norm.ppf(1 - alpha / 2)
    else:
        t_value = stats.t.ppf(1 - alpha / 2, n - 1)

    c1 = mean - t_value * (stdev / sqrt(n))
    c2 = mean + t_value * (stdev / sqrt(n))
    return c1, c2


def confidence_interval(X, alpha):
    """
    Calculates a confidence interval

    Arguments:
        X {List[float]} -- A list of measurements
        alpha {float} -- The confidence level

    Returns:
        Tuple[float, float] --  A tuple of two elements. The first one is the lower bound, the second one the upper bound
    """
    if alpha > 1 or alpha < 0:
        raise ValueError("alpha must be between 0 and 1")

    n = len(X)
    mean = statistics.mean(X)
    stdev = statistics.stdev(X)

    return confidence_interval_(n, mean, stdev, alpha)


def number_of_measurements(X, alpha, e):
    """
    Calculates the number of measurements required to keep the error for a confidence interval lower than an upper bound

    Arguments:
        X {List[float]} -- A list of measurements
        alpha {float} -- The confidence level for the confidence interval
        e {float} -- The maximum error as a value between 0 and 1

    Returns:
        int -- The raequired number of measurements
    """
    if alpha > 1 or alpha < 0:
        raise ValueError("alpha must be between 0 and 1")

    if e > 1 or e < 0:
        raise ValueError("e must be between 0 and 1")

    n = len(X)
    mean = statistics.mean(X)
    stdev = statistics.stdev(X)

    if n >= 30:
        t_value = stats.norm.ppf(1 - alpha / 2)
    else:
        t_value = stats.t.ppf(1 - alpha / 2, n - 1)

    return ceil(((t_value * stdev) / (mean * e)) ** 2)

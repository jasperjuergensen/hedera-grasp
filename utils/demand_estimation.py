import numpy as np


def demand_estimation(M):
    """Demand estimation

    Run the demand estimation. The demand matrix will be edited in place

    Arguments:
        M {np.ndarray} -- The initial demand matrix created using the stats
    """

    while True:
        pre = M[:, :, 1].copy()
        est_src(M)
        est_dst(M)
        post = M[:, :, 1].copy()
        if np.sum(np.absolute(pre - post)) == 0:
            # no demand changed
            break


def est_src(M):
    """Estimate the source demand

    Estimates the source demand. The demand matrix will be edited in place

    Arguments:
        M {np.ndarray} -- The current demand matrix
    """

    # sum up per src the demand of all flows, which are converged
    # steps:
    #   - mutliply the number of flows with the demand
    #   - multiply the converged flag with the demand
    #   - sum up per src
    df = np.sum(M[:, :, 2] * M[:, :, 0] * M[:, :, 1], axis=1)

    # count per src all flows, which are not converged
    # steps:
    #   - invert the converged flag
    #   - multiply the number of flows the the inverted converged flag
    #   - sum up per src
    nu = np.sum((1 - M[:, :, 2]) * M[:, :, 0], axis=1)

    # if there are no unconverged flows, there is nothing to do
    if np.sum(nu) == 0:
        return

    # calculate es per src, ignore division by zero warnings
    with np.errstate(divide="ignore"):
        es = (1 - df) / nu
    # set nan and inf values (because division by zero) to 0
    es[np.isnan(es) | np.isinf(es) | np.isneginf(es)] = 0

    # set es per src as demand for all flows, which are not converged
    # steps:
    #   - get all flow counts as one if there are flows and zero if there are no flows
    #   - multiply this with the reshaped es values
    #   - mutliply this with the inverted converged flag
    #   - add the original value for all those, which are converged
    M[:, :, 1] = (
        (M[:, :, 0] != 0).astype(int) * es.reshape((es.shape[0], -1)) * (1 - M[:, :, 2])
    ) + (M[:, :, 2] * M[:, :, 1])


def est_dst(M):
    """Estimate the destination demand

    Estimate the destination demand. The demand matrix will be edited in place

    Arguments:
        M {np.ndarray} -- The current demand matrix
    """

    # get rl per flow (if there are flows from src to dest or not)
    rl = (M[:, :, 0] != 0).astype(int)

    # create empty ds array
    ds = np.zeros((M.shape[1],))

    # sum up the demand per dest
    dt = np.sum(M[:, :, 1] * M[:, :, 0], axis=0)

    # count the number of flows per dest
    nr = np.sum(M[:, :, 0], axis=0)

    # check for each dt if it is greater than 1
    dt_greater_one = (dt > 1).astype(int)
    run_matrix = dt_greater_one.copy()

    # if all dt values are lower or equal than one return
    if np.sum(dt_greater_one) == 0:
        return

    # calculate es per dest, ignore division by zero warnings
    with np.errstate(divide="ignore"):
        es = 1 / nr
    # set nan and inf values (because division by zero) to 0
    es[np.isnan(es) | np.isinf(es) | np.isneginf(es)] = 0

    # when all dests are finished stop
    while True:

        # add the sum of flow demands with a demand lower than es and rl set
        # steps:
        #   - get all flows with demand lower than es as one
        #   - multiply it with the rl matrix
        #   - multiply it with the demand of the flows
        #   - multiply it with the number of flows
        #   - sum up per dest
        ds += np.sum(
            (M[:, :, 1] < es).astype(int) * rl * M[:, :, 1] * M[:, :, 0], axis=0
        )

        # sum up all flows where demand >= es and rl set
        # steps:
        #   - get all flows with demand >= es as 1
        #   - multiply it with the rl matrix
        #   - multiply it with the number of flows
        #   - only where run_matrix is set to 1
        #   - sum up per dest
        nr = np.sum(
            (M[:, :, 1] >= es).astype(int) * rl * M[:, :, 0] * run_matrix, axis=0
        )

        # get the sum of rl values per dest before changing
        pre = np.sum(rl, axis=0)

        # set the rl value to 0 where the demand is lower than es,
        # the rl value is set and the run_matrix value is set
        # steps:
        #   - keep rl value for
        #       - run_matrix not set OR
        #       - demand greater or equal es
        #   - all other values are automatically 0
        rl = ((1 - run_matrix) * rl) + ((M[:, :, 1] >= es).astype(int) * rl)
        # reset values which are greater than 1 to 1
        rl = (rl != 0).astype(int)

        # get the sum of rl values per dest after the change
        post = np.sum(rl, axis=0)

        # calculate new es, ignore division by zero warnings
        with np.errstate(divide="ignore"):
            new_es = (1 - ds) / nr
        # set nan values (because division by zero) to 0
        new_es[np.isnan(new_es) | np.isinf(new_es) | np.isneginf(new_es)] = 0
        # change the value only when run_matrix is set
        es = ((1 - run_matrix) * es) + (run_matrix * new_es)

        # where pre and post are different (change) set a 1, everywhere else a 0
        # don't change the run_matrix value from 0 to 1, therefor the multiplication with run_matrix
        run_matrix = (pre != post).astype(int) * run_matrix

        # if the sum is 0 there were no changes for any of the rl values
        if np.sum(run_matrix) == 0:
            # no changes to rl
            break

    # set demand to es where rl is set, dt_greater_one is set
    # steps:
    #   - multiply the es value
    #   - with the rl matrix
    #   - with the dt_greater_one matrix
    #   - keep old demand value where conditions not apply (inverse) but with OR connected
    M[:, :, 1] = (
        (es.reshape(-1, es.shape[0]) * rl * dt_greater_one)
        + ((1 - dt_greater_one) * M[:, :, 1])
        + ((1 - rl) * M[:, :, 1])
    )

    # set converged flag where rl is set and dt_greater_one is set
    M[:, :, 2] += rl * dt_greater_one
    # reset values which are greater than 1 to 1
    M[:, :, 2] = (M[:, :, 2] != 0).astype(int)

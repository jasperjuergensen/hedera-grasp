def switch_cores(h1, h2, state):
    """Switch cores

    Switches the cores in the state for h1 and h2

    Arguments:
        h1 {str} -- The first host
        h2 {str} -- The second host
        state {dict} -- The current state

    Returns:
        dict -- The new state
    """
    temp = state[h1]
    state[h1] = state[h2]
    state[h2] = temp
    return state

from ryu import cfg
from oslo_config.cfg import DictOpt

CONF = cfg.CONF

CONF.register_opts(
    [
        cfg.StrOpt(
            "hedera_algorithm",
            default="None",
            help="Algorithm to use for the flow scheduling.",
        ),
        cfg.IntOpt(
            "iterations",
            default=100,
            help="Number of iterations to run the hedera_algorithm for.",
        ),
        cfg.IntOpt(
            "interface_speed",
            default=100000000,
            help="Speed of host interfaces in bits per second.",
        ),
        cfg.ListOpt(
            "flat_host_list",
            required=True,
            help="List of all hostnames in the network, sorted by hostname.",
        ),
        cfg.IntOpt(
            "host_count",
            required=True,
            help="Number of hosts in the network. Must be the length of the flat_host_list.",
        ),
        cfg.ListOpt(
            "core_list", required=True, help="List of all core switches in the network."
        ),
        cfg.IntOpt(
            "core_count",
            required=True,
            help="Number of core switches in the network. Must be the length of the core_list.",
        ),
        cfg.ListOpt(
            "switch_list",
            required=True,
            help="List of all aggregation and edge switches in the network.",
        ),
        cfg.IntOpt(
            "pod_count", required=True, help="Number of pods in the fat tree topology."
        ),
        cfg.IntOpt(
            "pod_size",
            required=True,
            help="Number of hosts per pod in the fat tree topology. Should be equal to the pod_count in most cases.",
        ),
        cfg.IntOpt(
            "pod_edge_count", required=True, help="Number of edge switches per pod."
        ),
        DictOpt(
            "host_ip_dict",
            required=True,
            default={},
            help="Bidirectional mapping of hostnames to ip-addresses for all devices (switches and hosts)",
        ),
        cfg.IntOpt(
            "stats_frequency",
            default=5,
            help="Request stats from the switches and run the hedera algorithm every [value] seconds",
        ),
        cfg.StrOpt(
            "test",
            required=True,
            help="The test to run. Must be one of stride, staggered, random or random_bijektive",
        ),
        DictOpt("test_params", default={}, help="Parameters for the test."),
    ]
)

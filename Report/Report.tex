\documentclass[conference]{IEEEtran}
%Uses the IEEEtran.cls file which is already installed in overleaf
%Based on "IEEE Conference Template Example"
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\usepackage{listings}
\usepackage{hyperref}
\usepackage{pdfpages}

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2
}
\lstset{style=mystyle}


\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}
\begin{document}

\title{Using GRASP for Dynamic Flow Scheduling for Data Center Networks}

\author{\IEEEauthorblockN{Jasper Jürgensen}
\IEEEauthorblockA{\textit{Karlstad University}\\
Karlstad, Sweden \\
}
\and
\IEEEauthorblockN{Marcel Würsten}
\IEEEauthorblockA{\textit{Karlstad University}\\
Karlstad, Sweden \\
}
}
    
\maketitle

\begin{abstract}
In this report we rebuild a Hedera controller to compare the greedy randomized adaptive search procedure (GRASP) against Simulated Annealing used in the original Hedera paper. Our implementation uses OpenFlow 1.3 and Ryu as controller. We test and evaluate our implementation using Mininet.

\end{abstract}

\section{Introduction}
% Jasper, Marcel
In recent years, bandwidth demand in data centers has increased massively. Inter-rack network communication in particular becomes a bottleneck. A possible solution is to have multiple paths between two hosts. However, this leads to a series of new problems, e.g. uncertainties regarding path selection for specific flows. Possible solutions include equal cost multi-path (ECMP) \cite{b8} or Hedera \cite{b3}. The Hedera solution promises a higher bandwidth utilization by fixing some problems of ECMP (see section \ref{section:background_ECMP}) \cite{b3}.

\section{Objective}
% Jasper, Marcel
The authors of Hedera propose two solutions to calculate the optimal distribution of the elephant flows across the network: global first fit and Simulated Annealing \cite{b3}. Since Simulated Annealing is a meta-heuristic \cite{b9}, a replacement with other suitable meta-heuristics is possible. GRASP (greedy randomized adaptive search procedure) is commonly used for the constrained shortest path tour problem \cite{b1, b4}. The objective of this work is to implement a Hedera controller using GRASP as meta-heuristic and compare it to the Simulated Annealing approach as proposed in \cite{b3}. The goal is to see how the results differ with a different meta-heuristic. The two meta-heuristics in Hedera are compared against a simple ECMP algorithm.

\section{Background}

\subsection{ECMP}
\label{section:background_ECMP}
% Marcel
Equal-cost multi-path (ECMP) is a multi-path routing technique described by C. Hopps in RFC 2992 \cite{b8}. It is based on the autonomous system approach where each router decides on its own which of the possible next-hops to use when forwarding a packet. By calculating a hash over the packet header, the router can identify a flow. For N next-hops a unique region of the hashes' output space is assigned. The hop assigned to region in which the hash is located is taken. The ECMP algorithm tries to balance the flows over all the available routes. If this succeeds is directly related the the hashing function used. The ECMP algorithm with hashing consists of three parts: (1) the selection of regions for the next hops, (2) hash calculation and (3) choice of the next hop. The hash function used is configurable by the user. Using different hashing algorithms directly influences the performance of the algorithm. In contrast to a round-robin system, a flow follows the same path as long as the region boundaries are unchanged. This behaviour is preferred, since protocols such as TCP perform better if the path doesn't change on a open stream \cite{b10}. One problem for TCP is the change of the path MTU. If the path MTU changes with each packet, it limits the usefulness of path MTU discovery. Another problem are variable latencies. This leads to increased requirements regarding the buffering as separate paths can cause packets to always arrive out of order. This might also trigger the loss detection of TCP, which then retransmits that packet and consume extra bandwidth. Not all flows in a network require the same amount of bandwidth. Usually a minority of flows in a network transmit a huge amount of data, thereby consuming a significant amount of bandwidth. These flows are called elephant flows. In order to maximize a network's throughput, the route for such flows should be selected carefully. ECMP does not take this into account, risking substantial bandwidth loss due to collisions.

\subsection{Hedera}
% Marcel
Hedera is a dynamic flow scheduling algorithm for multi-stage switch topologies, proposed by M. Al-Fares et al. \cite{b3}. Such topologies can be found in data center networks. Compared to ECMP, Hedera collects statistics on flows from switches to have a complete overview on what happens on the network. Hedera uses a global controller. With this centralized view it can detect bottlenecks, that is local scheduler in each switch cannot. Hedera detects large flows to find a good path for them. Finding flow rules without exceeding any link's capacity is an NP-complete problem called multi-commodity-flow problem \cite{b12}. While for 3-stage Clos network an algorithm for polynomial time exists, there is no polynomial time algorithm known for 5-stage Clos networks like a 3-tier fat-tree-topology \cite{b11}. Therefore, Hedera uses heuristics that can be applied to a number of different data center topologies. Hedera tries to estimate the real network demand of each flow. Since the current flow sending rate is not very meaningful with regards to an ideal non-blocking network, it tries to find a hypothetical solution. In this flows are only limited by the senders or receivers NIC, on which the bandwidth is min-max-fair distributed between the flows. After calculating the demand, a flow placement algorithm is called. M. Al-Fares et al. \cite{b3} use Global First Fit and Simulated Annealing for this process. But Hedera is not limited to those two. To drastically reduce the amount of possible solutions, Hedera only assigns a single core switch per destination host rather than a core switch for each flow. After the routes are determined, Hedera then reroutes the traffic accordingly.


\subsection{Simulated Annealing}
\label{section:background_simulated-annealing}
% Marcel
Simulated Annealing is a heuristic algorithm to approximate the global minimum of a given function. It is based on the Metropolis-Hasting algorithm, a Markov chain Monte Carlo method proposed by N. Metropolis et al. \cite{b14}. The Metropolis-Hasting algorithm was gradually improved until Kirkpatrick et al. \cite{b9} succeeded in solving the traveling salesman problem and proposed the name Simulated Annealing. 

Simulated Annealing is based on concepts from physics, looking for an energetically most favorable state in a system witch can be described with the help of the Boltzmann distribution. Nowadays, there exist various versions of Simulated Annealing, but they all use a similar algorithm. An initial solution $x$ is chosen. In addition a monotonically decreasing sequence of positive temperature values $(T_t)_{t\in\mathbb{N}}$ needs to be defined. Then the algorithm iterates $t = 0$ through $t_{max}$. In each loop a random neighbor $y$ is selected. If the energy of this neighbour state $y$ is lower or equal to the energy of the state saved in $x$, the neighbour state is accepted and $x$ is set to $y$. If this is not the case, $y$ is randomly accepted as a new solution with a probability of $\exp(-\frac{energy(y) - energy(x)}{T_t})$. This means that the probability that $x$ is replaced with a worse $y$ decreases with larger deterioration $energy(y) - energy(x)$. As $T_t$ is a monotonically decreasing sequence, the probability decreases with each iteration. 

\subsection{GRASP}
\label{section:background_GRASP}
% Jasper
GRASP is a meta-heuristic originally proposed by Feo and Resende \cite{b5,b6}. It is a random multi-start local search heuristics \cite{b2}. Therefore, GRASP uses a new state per run, instead of operating on a single state. Each runs state is new initialized. GRASP can be split into two phases, construction and local search, which will be executed consecutively for each iteration \cite{b1,b4}. In the construction phase a new and partly random state is generated. For this purpose a so-called restricted candidate list (RCL) is created. This restricted list is created according to a greedy criterion. For each element it is searched for possible candidates. These candidates are evaluated how they influence the overall solution and then limited to some candidates. Next, one candidate is elected from this RCL. With these steps the initial state for the iteration is built up. When the initial state is built up the local search phase starts. In this phase, the local optimum for the initial state is searched. In each iteration the local optimum found in the local search phase is compared to the so far best solution. If it is better it will be taken as the new best solution. Contrary to Simulated Annealing, GRASP's local search is already satisfied by finding a local optimum. Multiple local optima from the construction state may converge to a global one.\\

\section{Architecture and Design}
\label{section:architecture_and_design}
% Jasper, Marcel
There are multiple parts to consider regarding architecture. The SDN controller can be considered as the "brain" of the network, as it decides what route should be used for a certain flow. The second part is a virtual network to simulate a suitable environment. The last part are the tests, with which the performance of the different algorithms is to be analyzed.

The controller is divided into several parts. One is an ECMP controller, which also functions independently of the rest as a Ryu controller. This ECMP controller answers requests from the switches and determines the route of a flow based on hashing values. It then installs a rule which allows further packets for this flow to be routed by the switch without consulting the controller.\\
In addition, there is a Hedera controller, which runs the ECMP controller for basic connectivity. The Hedera controller requests flow statistics from the switches and finds elephant flows. It then reroutes the flows on their way up to the assigned core switch in the network according to a chosen algorithm. To ensure appropriately diverted flows, Hedera rules should be prioritized higher than ECMP rules. The Hedera controller works independently of underlying algorithms and allows the easy exchange of these algorithms. For downwards connectivity from the core switch to the destination host, static flow table entries are created. These rules have the highest priority, so that connections within a pod will not be routed all the way up to a core switch even if the Hedera controller application has one assigned for these flows. In addition to all these rules, for OpenFlow 1.3 it is necessary to create flow table entries to send packets to then controller when no flow rule matches. These rules have the lowest possible priority, so that they match only if no other flow table entries match.\\

As a basis to test the controller, Mininet is used as a network simulator. The used network topology needs to have multiple paths between two hosts. For scientific purposes, the fat-tree-topology is commonly used for such evaluations. It is a multi-rooted tree that has multiple paths between hosts. The fat-tree-topology can be resized by the parameter $k$, with $k=4$ as the smallest useful version. To make the implementations of the controller applications a little bit easier, names and IP addresses follow certain conventions. A $k$-sized fat-tree topology is a three layer topology (core, aggregation, edge) in which
\begin{itemize}
    \item $k(k/2)^2$ hosts are distributed over $k$ pods
    \item each pod consists of $(k/2)^2$ hosts, $k/2$ edge switches and $k/2$ aggregation switches
    \item each edge switch connects to $k/2$ hosts and $k/2$ aggregation switches
    \item each aggregation switch connects to $k/2$ the edge switches in the same pod and $k/2$ core switches
    \item $(k/2)^2$ core switches are each connected to $k$ pods
\end{itemize}

For the Hedera controller application the algorithms only assign core switches to destination hosts instead of assigning a core switch for each flow. According to the Hedera paper this improves the performance for Simulated Annealing and GRASP in terms of runtime and results because it limits the search space \cite{b3}.\\

\section{Implementation}
\label{sec:implementation}

All these implementations are intended for testing purposes only and should not be deployed to production. They have a lot of constraints regarding network architecture and naming of devices.

\subsection{Mininet fat-tree-topology}
% Marcel
The implementation of a parametric fat-tree topology with size $k$ for Mininet is independent of the rest of the implementations. A constant naming schema was important (see section \ref{section:architecture_and_design}). 
When looking at the IP address scheme, each pod gets a /16 network and each edge switch together with the connected hosts a /24 network. These values were chosen, because they can provide sufficient amount of any of our testing capabilities. The IP addresses $10.x.y.z$ are directly mapped to the MAC address with the format 00:00:00:x:y:z. The datapath IDs used in Openflow follow this scheme in the format $w00000000xyz$ with each parameter as two digits.
The parameter $x,y,z,w$ were derived from the location of the node in the fat-tree topology with size $k$
\begin{itemize}
    \item core switch $\rightarrow$ $w = 01$, $x = k$, $y = core\;block\;id$, $z = core\;in\;block\;id$ with core block as a group of cores that are connected to the same aggregation switches
    \item aggregation switch $\rightarrow$ $w = 02$, $x = pod\;id$, $y = aggregation\;switch\;id$, $z = 1$ (reserved for further use)
    \item edge switch $\rightarrow$ $w = 02$, $x = pod\;id$, $y = edge\;switch\;id$, $z = 1$
    \item hosts $\rightarrow$ $x = pod\;id$, $y = edge\;switch\;id$, $z = server\;id$ (starting at 2)
\end{itemize}
For the hostnames the scheme was slightly modified but still has all information available:
\begin{itemize}
    \item \textit{sc-x} $\rightarrow$ core switch with id $x$
    \item \textit{sa-x-y} $\rightarrow$ aggregation switch in pod $x$ with id $y$
    \item \textit{se-x-y} $\rightarrow$ edge switch in pod $x$ with id $y$
    \item \textit{hxyz} $\rightarrow$ host in pod $x$ connected to edge switch $y$ and with id $z$, all three numbers as two digits with leading zero.
\end{itemize}

The fat-tree topology configures the network at startup. It disables IPv6 and creates static ARP entries on all hosts. It further sets the OpenFlow version on all switches to 1.3 and sets the downward flow table entries from the core to host, because these are static. The core switches have routes to the /16 networks of the pods, the aggregation switch have routes to the /24 networks of the pods and the edge switch have flow table entries for the hosts. Additionally, we add a flow table entry on lowest priority, so that the switch forwards flows without any other rules to the controller.


\subsection{ECMP}
% Jasper
The implementation using ECMP as scheduler is independent of the rest of the implementations. It is kept very simple. It takes the packet header and creates a MD5 hash over source and destination IP address, the source or destination port and the layer 4 protocol. If the IP packet contains an ICMP message instead of the source and destination port the ICMP type and code is used. The hexadecimal representation of the hash in then decoded as an integer value and divided modulo by the number of outgoing ports the switch has. This value is then used to decide which outgoing port is used for that flow. For flow is also a flow table entry created with the calculated port as outgoing port in the action.\\

\subsection{Hedera}
% Jasper, Marcel
On \href{https://www.github.com}{GitHub} there are implementations for a Hedera based flow scheduling with the Ryu controller available. These implementations mostly focus on global first fit and Simulated Annealing as proposed in the original Hedera paper \cite{b3}. Although these implementations were available, it was decided to implement the whole thing ourselves from the beginning. This has the advantage of not having to understand someone else's implementation and then try to plug your own implementation into it. Two implementations on GitHub were used as a reference and for inspiration for the own implementation: \href{https://github.com/Huangmachi/Hedera}{https://github.com/Huangmachi/Hedera} and \href{https://github.com/ederlf/Hedera-ryu}{https://github.com/ederlf/Hedera-ryu}.\\
For the implementation it was decided to use a modular approach because this has the advantage that individual components can be easily replaced. Especially for the flow placement algorithms, this is needed because one goal of this project was to have different ones compete against each other. There is one core component which is responsible for handling all of the communication with the switches with OpenFlow. The other components implement the different elements of the Hedera flow scheduling, mainly the demand estimation and the actual flow placement.\\

The demand estimation is based on the description in the Hedera paper \cite{b3} and the two GitHub implementations. In contrast to the pseudo-code in the Hedera paper \cite{b3} and the code in the two GitHub implementations, it is based on matrix operations using NumPy. With this implementation less loops are required because the source or the destination estimation can run for all hosts in the network at the same time instead of running in a loop for each separately.\\
One problem of the demand estimation implementation is that it does not reproduce the results presented in the Hedera paper \cite{b3}. Even when calculating the whole demand estimation by hand using the pseudo-code, the results differ from those in the paper in terms flow convergence at the end. The implementation by \href{https://github.com/Huangmachi/Hedera}{Huangmachi} has the same results like our implementation which differ from them in the paper. The implementation by \href{https://github.com/ederlf/Hedera-ryu}{ederlf} seems to be a very similar implementation but has a different result, which is equal to the one in the paper. In the end this difference should not have a big impact because it only effects which flows are converged in the end but not the demands the flows, which are used further in the application. For this reason is was decided to keep the current implementation with this deviation from the results in the original paper. This should not have any impact on the results of the measurements, but is a good point for further investigations.\\
For the actual flow placement two implementations are used: Simulated Annealing and GRASP. Both implementations are explained in more detail in the next two subsections.\\

The core Hedera component keeps track of the currently available switches in the network via the OpenFlow State Change messages. It sends a statistics request to all aggregation switches in the network at defined intervals (following the Hedera paper, e.g. every 5s). The individually arriving responses to this request are then aggregated together to get a statistic list of the currently active flows. It is ensured that there is only one entry in the aggregated statistics for the 5 tuples (source IP, source port, destination IP, destination port, layer 4 protocol) that define a flow. When all responses are received, the stats\_handling module filters out a list of large flows, called elephant flows. A flow is considered as large, if its consumed bandwidth (transmitted bytes divided by time) exceeds a threshold. Following the Hedera paper the threshold is set to 10\% of the host interface bandwidth. Based on these large flows, the demand matrix is created, which is then forwarded to the corresponding algorithm. The algorithm then returns which core switch the elephant flows to a specific destination should use. For each elephant flow, the OpenFlow rules on the edge switch connected to the source host and, the aggregation switch witch connects the edge switch with the assigned core switch, are either created or modified by the core component so that the flow is rerouted over the assigned core switch. The core component does not delete any flow rules. Instead, the rules of the ECMP controller are overruled using higher priorities in the rules placed by the core. Rules placed by the core component are kept in a list, so that the core component can check weather to install a new rule or just update an existing. In addition, all rules have an idle timeout and are deleted by the switch itself if not used. The core component is notified of this via Openflow Removed Messages, which allows it to keep its list of active rules up to date.

\subsection{Simulated Annealing}
% Jasper
The implementation of Simulated Annealing holds itself very close to the description in the Hedera paper \cite{b3}. In the startup phase of the controller application, the initial state for Simulated Annealing is created. This state creates a 1:1 mapping of cores to destination hosts. In this implementation for every host in a pod a different core switch is assigned. This causes a good distribution of hosts over the core switches and therefor flows over the available links in the network. After every update of the demand matrix Simulated Annealing is run for a number of rounds specified in the configuration. The generation of neighbour states is based on the three neighbour generators described in the Hedera paper \cite{b3}. The way these neighbour generators are implemented and the initial state is created has a very specific effect on the distribution of the host to core mapping. Because the neighbour generator only swaps the core switches for two hosts and in the initial state all hosts are equally distributed over all core switches, this equal distribution over all core switches is maintained for the whole time. The state of the last execution of Simulated Annealing is kept and used as the initial state for the next execution. This should limit the dependence on the initial state after a few execution round \cite{b3}.\\
One major part of the Simulated Annealing implementation is the energy function. Because this function is also used for GRASP and there were some problems with the performance of this function it is described in its own section in section \ref{subsec:energy_function}.\\

\subsection{GRASP}
% Jasper
The general idea for the implementation is based on the pseudo-code in two papers that use GRASP as a solution for the constrained shortest path problem \cite{b1, b4}. More details for the implementation are taken from a presentation given by one of the original inventors of GRASP \cite{b2}.\\
For the construction phase the implementation iterates over all hosts in the network, creates for the selected host a restricted candidate list (RCL) and selects randomly a core switch from the RCL. This so created state is then passed on to the local search. To create the RCL the implementation take the current state (where not all hosts have a core assigned) and iterates over all core switches. For each core switch an assignment is created. This new created state is then run through the energy function to get the energy the state would have if the selected core would be assigned for the selected host. After this is done the list of cores is limited using the so called minmax $\alpha$-percentage based RCL \cite{b2}. For this all candidates are put into RCL which have an energy that is lower than $min(energy) + (1 - \alpha) * [max(energy) * min(energy)]$, where $\alpha$ is a configurable parameter by default set to $0.4$.\\
In the local search phase it is tried to improve the state created in the construction phase. First neighbour states are created. The implementation used only creates 10 neighbours using parts of the neighbour function used by Simulated Annealing. This implementation is not optimal, but was chosen to limit the effort for the implementation and reduce the complexity as there was not much time. For all of these neighbour states the energy is calculated. If the best neighbour state is better than the current best state (in the first round the initial state), it is taken as the new best state and a new local search round is started based on this new best state. This is done until no further improvement is achieved.\\

\subsection{Energy function}
\label{subsec:energy_function}
% Jasper
There is an own subsection on the energy function because the implementation of this function has a great impact on the computational performance of Simulated Annealing and GRASP. This function is responsible for at least 99\% of the computational time needed by GRASP because it is called in a single GRASP run hundreds of time. This energy function is used by both, GRASP and Simulated Annealing.\\
The role of the energy function is to measure how good a solution is for the problem. In our case it measures how good the found host-to-core-mapping distributes the demands over all available links. Following the description of their implementation in the original Hedera paper \cite{b3} the implementation sums up the demands of all links that are exceeded. This means that the smaller the result of the energy function, the better the result. The implementation of all other parts in GRASP and Simulated Annealing must take this into account and try to find a solution with the smallest energy.\\
The first version of this energy function was based on matrix operations using NumPy (\href{https://numpy.org/}{numpy.org}). This implementation was very fast, but it had several problems. The first problem was, that the implementation was very complex. Even with some comments the author of the code was not able to understand what it does after a few days not working on it. The second problem was, that the implementation had some constraints on how the host-to-core-mapping needs to look like, that were fulfilled by Simulated Annealing but not by GRASP.\\
The second version was iterating over all switches (and therefor over all links between switches) and calculated for each link which flows go over this link. The problem with this implementation was, that it needed far too much loops and was therefor too slow. With the first version a single run of Simulated Annealing with $100$ iterations in a $k=4$ fat-tree-topology\footnote{All following tests in this subsection are conducted with the same topology} took about $0.04$ seconds. With the second version it took about $1.4$ seconds, an increase by a factor of $35$. The GRASP implementation with $20$ neighbours and $100$ iterations did not finish in a reasonable time.\\
To reduce the number of iterations the third version does only iterate over all source-destination-host-pairs. This reduces the number of iterations a little bit, but is still dependent on the size of the network. This speeds up the runtime so that GRASP needs with 20 neighbours and 100 iterations only about three seconds. For further speedup it was decided to implement the energy function in Cython (\href{https://cython.org/}{cython.org}) as a pyx file and then compile it to a shared object that can be used in the python code. Because Cython has some special extensions for compiling functions with NumPy, this could be easily implemented in a good way. The shared object then saves all the calls to the python interpreter and is therefor a little bit faster. GRASP with $20$ neighbours and $100$ iterations needs with this only about $1.4$ seconds, so this is again a speedup by a factor of two.\\

\section{Evaluation}

\subsection{Benchmarks}

To run the benchmarks, a data center network is needed. Because a real data center network for testing purposes was not available, the Mininet (\href{https://mininet.org}{mininet.org}) was used for an emulated network. As a network topology a fat-tree \cite{b7} was used.\\
As controller software the Ryu controller (\href{https://ryu-sdn.org/}{ryu-sdn.org}) was chosen.\\

\subsubsection{Benchmark communication suite}
% Marcel
Due to the unavailability of real data flows and communication pattern from commercial data centers, we have created the following communication pattern similar to the pattern in \cite{b13, b3}.
\begin{enumerate}
    \item Stride($i$): a host with index $x$ connects to host with the index $(x+i)mod(\#hosts)$  
    \item Staggered($EdgeP$,$PodP$): a host connects
    \begin{itemize}
        \item with probability $EdgeP$ to a host in the same edge 
        \item with probability $PodP$ to a host in the same pod 
        \item with probability $1 - EdgeP\;-\;PodP$ to a host outside of the same pod 
    \end{itemize}
    \item Random: a host connects to another random host in the network
    \item Random bijective: a host connects to another random host in the network and this host connects back to the host to create a bijective mapping
\end{enumerate}

While the pattern were derived from \cite{b13}, we define that each hosts initiates one connection. 
    
\subsubsection{Benchmark setup}
% Jasper

For the benchmark two differently sized fat-tree-topologies were used. All tests were conducted with a $k=4$ fat-tree-topology and repeated $10$ times. Because of the higher runtime and resource consumption, no tests were conducted with greater networks.\\
All executed tests run iperf for $60$ seconds. For the evaluation only the data in the middle $40$ seconds were take. This is the same like in the Hedera paper \cite{b3}. The bandwidth was measured every $10$ seconds on the destination host. From these measurements the mean was taken for every host and then the mean over all hosts. It is assumed, that Al-Fares et al. do a similar thing but describe this as "\textit{aggregate network utilization - bisection bandwidth}" \cite{b3}.\\

\subsection{Results}
% Jasper

The results in figure \ref{fig:algorithm_alternatives} shows, that it was not possible to reproduce the promising results of the Hedera paper \cite{b3}. All results of Hedera with Simulated Annealing are on the same level as ECMP, if not worse. For stride $4$ and stride $8$ the results of ECMP are statistically significant but not much better than the results for Simulated Annealing. The results for GRASP are mainly on the same level as the results for Simulated Annealing. Only for stride $2$ the results for GRASP are visibly but not statistically significant better than the results for ECMP and Simulated Annealing. For stride $4$ GRASP is a little better than Simulated Annealing but still worse than ECMP.\\

\begin{figure*}
    \centering
    \includegraphics[scale=0.75]{annealing_vs_grasp_vs_ecmp_mean.png}
    \caption{Simulated Annealing vs. GRASP vs. ECMP average mean bandwidth with error margins for 95\% confidence intervals}
    \label{fig:algorithm_alternatives}
\end{figure*}

It was also tested if an increase in the number of iterations for Simulated Annealing changes something in these results. As depicted in figure \ref{fig:annealing_100_vs_200_vs_1000} there is no statistically significant difference between $100$ iterations (used in all other tests), $200$ iterations or $1000$ iterations. This was also confirmed by the means of differences test. The tests for this question were executed in the $k=4$ fat-tree-topology only with the stride flow patterns and for 10 repetitions. Because the runtime for Simulated Annealing increases linear with the number of iterations, this increase by a factor $10$ causes an increase in runtime by the factor $10$. So instead of $0.02$ seconds per execution of Simulated Annealing this needs about $0.2$ second.\\

\begin{figure}
    \centering
    \includegraphics[scale=0.6]{annealing_100_vs_1000_vs_200_mean.png}
    \caption{Simulated Annealing 100 vs. 200 vs. 1000 iterations average mean bandwidth with error margins for 95\% confidence intervals}
    \label{fig:annealing_100_vs_200_vs_1000}
\end{figure}

For GRASP it was also tested if some changes in the parameters has an impact on the results. Tested parameters include an increase in the number of iterations from $100$ to $200$. For $200$ iterations it was also tested, if an increase in the number of generated neighbours in the local search phase has any impact. As shown in figure \ref{fig:grasp_100_vs_200_vs_30} there is also no statistically significant difference. Changing the parameters has an even stronger influence on the runtime with GRASP than with Simulated Annealing. For the case with $200$ iterations and $30$ neighbours a single execution of GRASP needs about three seconds which is not much less than the magical boundary of five seconds for the time between two statistic requests in the Hedera application.\\

\begin{figure}
    \centering
    \includegraphics[scale=0.6]{grasp_100_vs_200_vs_30_mean.png}
    \caption{GRASP 100 vs. 200 iterations and 10 vs. 30 neighbours average mean bandwidth with error margins for 95\% confidence intervals}
    \label{fig:grasp_100_vs_200_vs_30}
\end{figure}

\section{Conclusion}
% jasper

The results are definitely not as hoped. It was not possible to reproduce the results from the Hedera paper \cite{b3} in terms of the big advantage Hedera with Simulated Annealing should have over ECMP. It was also not possible to improve these results using GRASP as a different meta-heuristic.\\
These results show, that there is a lot of room for optimization. As stated in various parts of the implementation (see section \ref{sec:implementation}), this room for optimization includes the implementation. But also for the choice of parameters a lot of optimization can be done. The tests performed are scratching the surface only. Despite the non-optimized state, GRASP is on the same level with Simulated Annealing. Whether further improvements can be made with an optimized implementation and optimized parameters is a question for future research.\\

\section{Repository}
The project repository is available at \href{https://gitlab.com/jasperjuergensen/hedera-grasp}{https://gitlab.com/jasperjuergensen/hedera-grasp}

\begin{thebibliography}{00}

\bibitem{b1} D. Ferone, P. Festa, and F. Guerriero, “An efficient exact approach for the constrained shortest path tour problem,” Optimization Methods and Software, vol. 35, no. 1, pp. 1–20, Jan. 2020, doi: 10.1080/10556788.2018.1548015.
\bibitem{b2} M. G. C. Resende, “GRASP: Greedy Randomized Adaptive Search Procedures A metaheuristic for combinatorial optimization,” presented at the ECCO’2000, Capri, Italy, May 2000.
\bibitem{b3} M. Al-Fares, S. Radhakrishnan, B. Raghavan, N. Huang, and A. Vahdat, “Hedera: Dynamic Flow Scheduling for Data Center Networks,” in Proceedings of the 7th USENIX Conference on Networked Systems Design and Implementation, USA, 2010, p. 19.
\bibitem{b4} D. Ferone, P. Festa, F. Guerriero, and D. Laganà, “The constrained shortest path tour problem,” Computers \& Operations Research, vol. 74, pp. 64–77, Oct. 2016, doi: 10.1016/j.cor.2016.04.002.
\bibitem{b5} T. A. Feo and M. G. C. Resende, “A probabilistic heuristic for a computationally difficult set covering problem,” Operations Research Letters, vol. 8, no. 2, pp. 67–71, Apr. 1989, doi: 10.1016/0167-6377(89)90002-3.
\bibitem{b6} T. A. Feo and M. G. C. Resende, “Greedy Randomized Adaptive Search Procedures,” J Glob Optim, vol. 6, no. 2, pp. 109–133, Mar. 1995, doi: 10.1007/BF01096763.
\bibitem{b7} C. E. Leiserson, “Fat-trees: Universal networks for hardware-efficient supercomputing,” IEEE Trans. Comput., vol. C–34, no. 10, pp. 892–901, Oct. 1985, doi: 10.1109/TC.1985.6312192.
\bibitem{b8} C. Hopps, “Analysis of an Equal-Cost Multi-Path Algorithm.”, RFC 2992, IETF, 2000.
\bibitem{b9} S. Kirkpatrick, C. D. Gelatt, Jr., and M. P. Vecchi, “Optimization by Simulated Annealing,” Science, vol. 220, no. 4598, May 1983.
\bibitem{b10} D. Thaler, C. Hopps, “Multipath Issues in Unicast and Multicast Next-Hop Selection”, RFC 2991, IETF, 2000.
\bibitem{b11} K. Holmberg, “Optimization Models for Routing in Switching Networks of Clos Type with Many Stages,” AMO, vol. 10, no. 1, p. 24, 2008.
\bibitem{b12} Even, Shimon and Itai, Alon and Shamir, Adi, “On the complexity of time table and multi-commodity flow problems” sfcs, pp. 184-193, 1975.
\bibitem{b13} M. Al-Fares, A. Loukissas, A. Vahdat, “A Scalable, Commodity Data Center Network Architecture” ACM SIGCOOM computer communication review, pp. 63-74, 2008.
\bibitem{b14} N. Metropolis, A. W. Rosenbluth, M. N. Rosenbluth, A.H. Teller, E. Teller, “Equation of state calculations by fast computing machines” The journal of chemical physics, 21(6), pp. 1087-1092, 1953, doi: 10.1063/1.1699114

\end{thebibliography}

\clearpage
\section*{Attachment}
\includepdf[pages=-]{who_did_what.pdf}

\end{document}
